package com.precorconnect.claimspiffservice.objectmodel;

import java.sql.Timestamp;

public interface SpiffClaimedDate {

	Timestamp getValue();
}
