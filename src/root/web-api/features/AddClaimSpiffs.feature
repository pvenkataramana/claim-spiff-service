Feature: Add Claim SPIFF
Add selected Claim SPIFF
   
     Background:
    Given a claimSpiffDtoList consists of:
      | attribute     				| validation | type   |
      | spiffEntitlementId          | required   | number |
      | partnerSaleRegistrationId 	| required   | number |
      | accountId     				| required   | string |
      | userId  					| required   | string |
      | installDate   				| required   | date   |
      | spiffAmount   				| required   | number |
      | spiffClaimedDate 			| optional   | string |
      | facilityName  				| required   | string |
      | invoiceNumber   			| required   | string |
       
    
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid claimSpiffDtoList
    When I execute addCliamSpiffs
    Then claimSpiffDtoList is added to the claimspiffs database with attributes:
      | attribute     			    | value            							|
      | claimId                     | (auto generated) 							|
      | partnerSaleRegistrationId 	|  claimSpiffDto.partnerSaleRegistrationId 	|
      | accountId     				|  claimSpiffDto.accountId 					|
      | userId  					|  claimSpiffDto.userId 					|
      | installDate   				|  claimSpiffDto.installDate   				|
      | spiffAmount   				|  claimSpiffDto.spiffAmount 				|
      | spiffClaimedDate 			|  claimSpiffDto.spiffClaimedDate 			|
      | facilityName  				|  claimSpiffDto.facilityName 				|
      | invoiceNumber   			|  claimSpiffDto.invoiceNumber 				|
