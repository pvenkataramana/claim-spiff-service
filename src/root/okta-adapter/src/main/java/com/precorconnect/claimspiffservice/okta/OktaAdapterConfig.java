package com.precorconnect.claimspiffservice.okta;

import java.net.URL;

import com.precorconnect.OAuth2ClientCredentials;

public interface OktaAdapterConfig {

    URL getOktaBaseUrl();

    OktaApiToken getOktaApiToken();

    OAuth2ClientCredentials getPrecorConnectClientCredentials();


}
