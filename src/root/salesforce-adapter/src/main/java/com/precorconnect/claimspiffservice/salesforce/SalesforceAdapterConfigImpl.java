package com.precorconnect.claimspiffservice.salesforce;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2ClientCredentials;
import com.precorconnect.Password;
import com.precorconnect.Username;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfig;

public class SalesforceAdapterConfigImpl
        implements SalesforceAdapterConfig {

    /*
    fields
     */
    private final URL baseOAuth2Url;

    private final URL instanceBaseUrl;

    private final Username username;

    private final Password password;

    private final OAuth2ClientCredentials connectedAppCredentials;

    private final OAuth2ClientCredentials precorConnectClientCredentials;

    private final IdentityServiceSdkConfig identityServiceSdkConfig;


    /*
    constructors
     */
    public SalesforceAdapterConfigImpl(
            @NonNull URL baseOAuth2Url,
            @NonNull URL instanceBaseUrl,
            @NonNull Username username,
            @NonNull Password password,
            @NonNull OAuth2ClientCredentials connectedAppCredentials,
            @NonNull OAuth2ClientCredentials precorConnectClientCredentials,
            @NonNull IdentityServiceSdkConfig identityServiceSdkConfig
    ) {

        this.baseOAuth2Url =
				guardThat(
						"baseOAuth2Url",
						baseOAuth2Url
				)
						.isNotNull()
						.thenGetValue();

        this.instanceBaseUrl =
				guardThat(
						"instanceBaseUrl",
						instanceBaseUrl
				)
						.isNotNull()
						.thenGetValue();

        this.username =
				guardThat(
						"username",
						username
				)
						.isNotNull()
						.thenGetValue();

        this.password =
				guardThat(
						"password",
						password
				)
						.isNotNull()
						.thenGetValue();

        this.connectedAppCredentials =
				guardThat(
						"connectedAppCredentials",
						connectedAppCredentials
				)
						.isNotNull()
						.thenGetValue();

        this.precorConnectClientCredentials =
				guardThat(
						"precorConnectClientCredentials",
						precorConnectClientCredentials
				)
						.isNotNull()
						.thenGetValue();

        this.identityServiceSdkConfig =
				guardThat(
						"identityServiceSdkConfig",
						identityServiceSdkConfig
				)
						.isNotNull()
						.thenGetValue();


    }

    /*
    getter methods
     */
    @Override
    public URL getBaseOAuth2Url() {
        return baseOAuth2Url;
    }

    @Override
    public URL getInstanceBaseUrl() {
        return instanceBaseUrl;
    }

    @Override
    public Username getUsername() {
        return username;
    }

    @Override
    public Password getPassword() {
        return password;
    }

    @Override
    public OAuth2ClientCredentials getConnectedAppCredentials() {
        return connectedAppCredentials;
    }

    @Override
    public OAuth2ClientCredentials getPrecorConnectClientCredentials() {
        return precorConnectClientCredentials;
    }

    @Override
    public IdentityServiceSdkConfig getIdentityServiceSdkConfig() {
        return identityServiceSdkConfig;
    }

    /*
    equality methods
    */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((baseOAuth2Url == null) ? 0 : baseOAuth2Url.hashCode());
		result = prime
				* result
				+ ((connectedAppCredentials == null) ? 0
						: connectedAppCredentials.hashCode());
		result = prime
				* result
				+ ((identityServiceSdkConfig == null) ? 0
						: identityServiceSdkConfig.hashCode());
		result = prime * result
				+ ((instanceBaseUrl == null) ? 0 : instanceBaseUrl.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime
				* result
				+ ((precorConnectClientCredentials == null) ? 0
						: precorConnectClientCredentials.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SalesforceAdapterConfigImpl other = (SalesforceAdapterConfigImpl) obj;
		if (baseOAuth2Url == null) {
			if (other.baseOAuth2Url != null) {
				return false;
			}
		} else if (!baseOAuth2Url.equals(other.baseOAuth2Url)) {
			return false;
		}
		if (connectedAppCredentials == null) {
			if (other.connectedAppCredentials != null) {
				return false;
			}
		} else if (!connectedAppCredentials
				.equals(other.connectedAppCredentials)) {
			return false;
		}
		if (identityServiceSdkConfig == null) {
			if (other.identityServiceSdkConfig != null) {
				return false;
			}
		} else if (!identityServiceSdkConfig
				.equals(other.identityServiceSdkConfig)) {
			return false;
		}
		if (instanceBaseUrl == null) {
			if (other.instanceBaseUrl != null) {
				return false;
			}
		} else if (!instanceBaseUrl.equals(other.instanceBaseUrl)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (precorConnectClientCredentials == null) {
			if (other.precorConnectClientCredentials != null) {
				return false;
			}
		} else if (!precorConnectClientCredentials
				.equals(other.precorConnectClientCredentials)) {
			return false;
		}
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}



}
