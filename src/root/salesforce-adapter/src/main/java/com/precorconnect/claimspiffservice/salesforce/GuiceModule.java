package com.precorconnect.claimspiffservice.salesforce;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final SalesforceAdapterConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull SalesforceAdapterConfig config
    ) {

        this.config =
				guardThat(
						"config",
						config
				)
						.isNotNull()
						.thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFactories();

        bindServices();

        bindFeatures();

    }

    private void bindFactories() {

        bind(OAuth2AccessTokenFactory.class)
                .to(OAuth2AccessTokenFactoryImpl.class);

    }

    private void bindServices() {


    }

    private void bindFeatures() {

        bind(GetAccountNameWithIdFeature.class)
                .to(GetAccountNameWithIdFeatureImpl.class);

    }

    @Provides
    SalesforceAdapterConfig salesforceAdapterConfig() {
        return config;
    }

    @Provides
    @Singleton
    @InjectSalesforceOAuth2WebTarget
    WebTarget provideSalesforceOAuth2WebTarget(
            ObjectMapperProvider objectMapperProvider
    ) {

        try {

            return
                    ClientBuilder
                            .newClient()
                            .register(objectMapperProvider)
                            .register(JacksonFeature.class)
                            .target(config.getBaseOAuth2Url().toURI());

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

    @Provides
    @Singleton
    @InjectSalesforceServicesDataV34WebTarget
    WebTarget provideSalesforceServicesDataV34WebTarget(
            @NonNull ObjectMapperProvider objectMapperProvider
    ) {

        try {

            return
                    ClientBuilder
                            .newClient()
                            .register(objectMapperProvider)
                            .register(JacksonFeature.class)
                            .target(config.getInstanceBaseUrl().toURI())
                            .path("services/data/v34.0");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }


    @Provides
    @Singleton
    IdentityServiceSdk identityServiceSdk() {

        return
                new IdentityServiceSdkImpl(
                        config.getIdentityServiceSdkConfig()
                );

    }

}
