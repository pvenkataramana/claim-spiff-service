## Description  
Precor Connect service responsible for claim spiffs.

## Features

##### Add List of Claim Spiffs
* [documentation](src/root/api/features/AddClaimSpiffs.feature)

##### List Claim Spiffs With PartnerId
* [documentation](src/root/api/features/GetClaimSpiffsWithIds.feature)

##### Create Spiff Entitlements
* [documentation](src/root/api/features/CreateSpiffEntitlements.feature)

##### List Spiff Entitlements With Id
* [documentation](src/root/api/features/ListEntitlementsWithPartnerId.feature)

##### Update Invoice Url
* [documentation](src/root/api/features/UpdateInvoiceUrl.feature)

##### Update Partner Rep
* [documentation](src/root/api/features/UpdatePartnerRep.feature)

##### List Spiff Logs
* [documentation]

##### List Spiff Logs With Account Id
* [documentation]

##### Update Spiff Log
* [documentation]


## APIs
* [REST API](src/root/web-api/README.md)  

## SDKs  
* [SDK for Java](src/root/sdk/README.md)  
* [SDK for Javascript](https://bitbucket.org/precorconnect/claim-spiff-service-sdk-for-javascript)

## Configuration
Configuration is obtained through the environment variables listed below.
note: environment variables prefixed with `TEST_` are used only in integration tests.

|name|
|---|
|DATABASE_URI|
|DATABASE_USERNAME|
|DATABASE_PASSWORD|
|PRECOR_CONNECT_API_BASE_URL|
|TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY|

## Develop:

#### Software
- git
- java 8 JDK
- maven
- docker toolbox

#### Scripts

set environment variables (perform prior to running or integration testing locally)
```PowerShell
 .\src\root\set-environment.ps1
```

compile & unit test
```PowerShell
mvn test -f .\src\root\pom.xml
```

compile & integration test
```PowerShell
mvn integration-test -f .\src\root\pom.xml
```

create docker image
```PowerShell
 mvn install -f .\src\root\pom.xml
```

run
```PowerShell
java -jar .\src\root\web-api\target\web-api.jar
```

run in docker container
```PowerShell
docker run -P claim-spiff-service
```

## claim-spiff-service sample REST request and response formats

1) AddClaimSpiffs

REQUEST:-->

	https://api-dev.precorconnect.com/claim-spiffs/

HEADER:-->

	Content-Type: application/json
	
	Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


METHOD:-->

	POST

REQUESTBODY:

	[
		{
		    "spiffEntitlementId":1,
			"partnerSaleRegistrationId": 233,
			"partnerAccountId": "001K000001H2Km2IAF",
			"partnerRepUserId": "124",
			"installDate": "12/26/1986",
			"spiffAmount": 300,
			"facilityName": "krish",
			"invoiceNumber": "124"
		}
	]


RESPONSE:

	Status Code: 200 OK

    [
        1
    ]

-----------------------------------------------------------------------

2) GetClaimSpiffsWithIds

REQUEST:-->

	https://api-dev.precorconnect.com/claim-spiffs/claimids/1,2

HEADER:-->

	Content-Type: application/json
	Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I

METHOD:-->

	GET

RESPONSE:

	Status Code: 200 OK

RESPONSEBODY:

    [
        {
            "claimId": 1,
            "partnerSaleRegistrationId": 232,
            "partnerAccountId": "001K000001H2Km2IAF",
            "partnerRepUserId": "123",
            "installDate": "11/21/1985",
            "spiffAmount": 200,
            "spiffClaimedDate": "12/12/2015 14:02:18",
            "facilityName": "ram",
            "invoiceNumber": "123"
        },
        {
            "claimId": 2,
            "partnerSaleRegistrationId": 233,
            "partnerAccountId": "001K000001H2Km2IAF",
            "partnerRepUserId": "124",
            "installDate": "12/26/1986",
            "spiffAmount": 300,
            "spiffClaimedDate": "12/12/2015 14:50:01",
            "facilityName": "krish",
            "invoiceNumber": "124"
        }
    ]
	
---------------------------------------------------------------------

3) CreateSpiffEntitlements

REQUEST:-->

	https://api-dev.precorconnect.com/claim-spiffs/entitlements

HEADER:-->

	Content-Type: application/json

METHOD:-->

	POST

REQUESTBODY:

	[
		{
			"accountId": "001K000001H2Km2IAF",
			"partnerSaleRegistrationId": 3125,
			"facilityName": "krish",
			"invoiceNumber": 1113113,
			"invoiceUrl": "xyz",
			"partnerRepUserId": 124,
			"installDate": "12/22/1986",
			"spiffAmount": 800
		}
	]

RESPONSE:

	Status Code: 200 OK

    [
        1
    ]

----------------------------------------------------------------------------------------------------------

4) ListEntitlementsWithPartnerId

REQUEST:-->

	https://api-dev.precorconnect.com/claim-spiffs/id/001K000001H2Km2IAF


METHOD:-->

	GET

RESPONSE:

	Status Code: 200 OK

    [
        {
            "spiffEntitlementId": 4,
            "partnerAccountId": 232,
            "partnerSaleRegistrationId": 3124,
            "facilityName": "rams",
            "invoiceNumber": 1113112,
            "invoiceUrl": "sampleurl",
            "partnerRepUserId": 495,
            "installDate": "21/11/1985",
            "spiffAmount": 700
        }
    ]
	
------------------------------------------------------------------------------------------------------------

5) UpdateInvoiceUrl

REQUEST:-->
	
	https://api-dev.precorconnect.com/claim-spiffs/entitlements/3124/invoiceurl


METHOD:-->

	POST


REQUESTBODY:

	http://www.testinvoice.com

RESPONSE:

	Status Code: 200 OK

-------------------------------------------------------------------------------------------------------------

6) UpdatePartnerRep

REQUEST:-->

	https://api-dev.precorconnect.com/claim-spiffs/entitlements/3124/partnerrepuserid/788

METHOD:-->

	POST

RESPONSE:

	Status Code: 200 OK

-------------------------------------------------------------------------------------------------------------

7) ListSpiffLogs


REQUEST:-->
	
	https://api-dev.precorconnect.com/spiff-logs


METHOD:-->

	GET



RESPONSE:

	Status Code: 200 OK

    [
  
     {
            "claimId": 1,
            "partnerSaleRegistrationId": 123,
            "partnerAccountId": "001K000001H2Km2IAF",
            "partnerRepUserId": "00u5i5aimx8ChKQvB0h7",
            "sellDate": "11/24/2015",
            "installDate": "11/24/2015",
            "spiffAmount": 120,
            "spiffClaimedDate": "01/08/2016 12:25:54",
            "facilityName": "Precor",
            "invoiceNumber": "11111",
            "reviewStatus": "Un-Reviewed",
            "reviewedBy": null,
            "reviewedDate": null

      }

     ]

-------------------------------------------------------------------------------------------------------------

8) ListSpiffLogsWithId

REQUEST:-->
	
	https://api-dev.precorconnect.com/spiff-logs/001K000001H2Km2IAF


METHOD:-->

	GET



RESPONSE:

	Status Code: 200 OK

    [
  
     {
           "claimId": 1,
           "partnerSaleRegistrationId": 123,
           "partnerAccountId": "001K000001H2Km2IAF",
           "partnerRepUserId": "00u5i5aimx8ChKQvB0h7",
           "sellDate": "11/24/2015",
           "installDate": "11/24/2015",
           "spiffAmount": 120,
           "spiffClaimedDate": "01/08/2016 12:25:54",
           "facilityName": "Precor",
           "invoiceNumber": "11111",
           "reviewStatus": "Un-Reviewed",
           "reviewedBy": null,
           "reviewedDate": null

      }

     ]

-------------------------------------------------------------------------------------------------------------

9)   updateSpiffLog

REQUEST:-->
	
	https://api-dev.precorconnect.com/spiff-logs/update


METHOD:-->

	POST


REQUESTBODY:


     [
  
     {
           "claimId": 1,
           "partnerSaleRegistrationId": 123,
           "partnerAccountId": "001K000001H2Km2IAF",
           "partnerRepUserId": "00u5i5aimx8ChKQvB0h7",
           "sellDate": "11/24/2015",
           "installDate": "11/24/2015",
           "spiffAmount": 120,
           "spiffClaimedDate": "01/08/2016 12:25:54",
           "facilityName": "Precor",
           "invoiceNumber": "11111",
           "reviewStatus": "Un-Reviewed",
           "reviewedBy": null,
           "reviewedDate": null

      }

     ]




RESPONSE:

	Status Code: 200 OK