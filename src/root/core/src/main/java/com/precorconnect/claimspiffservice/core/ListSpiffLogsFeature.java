package com.precorconnect.claimspiffservice.core;

import java.util.Collection;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;


public interface ListSpiffLogsFeature {

	Collection<SpiffLogView> listSpiffLogs(
			) throws AuthenticationException;

}
