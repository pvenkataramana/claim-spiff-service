package com.precorconnect.claimspiffservice.salesforce;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * discriminates a {@link javax.ws.rs.client.WebTarget} as a salesforce oauth2 web target
 * see: https://github.com/google/guice/wiki/BindingAnnotations
 */
@BindingAnnotation
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface InjectSalesforceOAuth2WebTarget {
}
