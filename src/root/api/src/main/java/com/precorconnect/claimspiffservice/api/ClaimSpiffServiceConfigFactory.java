package com.precorconnect.claimspiffservice.api;

public interface ClaimSpiffServiceConfigFactory {

	ClaimSpiffServiceConfig construct();
}
