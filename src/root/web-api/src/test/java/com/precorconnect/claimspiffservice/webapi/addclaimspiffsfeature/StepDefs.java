package com.precorconnect.claimspiffservice.webapi.addclaimspiffsfeature;

import static com.jayway.restassured.RestAssured.given;

import java.util.Collection;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.claimspiffservice.webapi.Config;
import com.precorconnect.claimspiffservice.webapi.ConfigFactory;
import com.precorconnect.claimspiffservice.webapi.Dummy;
import com.precorconnect.claimspiffservice.webapi.Factory;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepDefs
		extends AbstractSpringIntegrationTest {

    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    private OAuth2AccessToken accessToken;

    private Collection<ClaimSpiffWebDto> claimSpiffWebDtos;

    private Response responseOfPostToSpiffEntitlements;


    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
	@Given("^a claimSpiffDtoList consists of:$")
	public void aclaimSpiffDtoListconsistsof(
			DataTable arg1
			) throws Throwable {
		// no op, we are creating spiffentitlementdtolist in dummy
	}

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
    ) throws Throwable {

        accessToken = new OAuth2AccessTokenImpl(
                								factory
                									.constructValidPartnerRepOAuth2AccessToken()
                									.getValue()
                								);

    }

    @Given("^provide a valid claimSpiffDtoList$")
    public void provideavalidclaimSpiffDtoList(
    ) throws Throwable {

    	claimSpiffWebDtos =
                	dummy.getClaimSpiffWebDtoList();

    }

    @When("^I execute addCliamSpiffs$")
    public void IExecuteaddCliamSpiffs(
    ) throws Throwable {

    	responseOfPostToSpiffEntitlements =
    			given()
                .contentType(ContentType.JSON)
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                accessToken
                                        .getValue()
                        )
                )
                .body(claimSpiffWebDtos)
                .post("/claim-spiffs");

    }

    @Then("^claimSpiffDtoList is added to the claimspiffs database with attributes:$")
    public void anspiffentitlementisaddedtothespiffentitlementdatabase(
    		DataTable arg
    ) throws Throwable {

    	responseOfPostToSpiffEntitlements
									.then()
									.assertThat()
									.statusCode(200);

    }


}
