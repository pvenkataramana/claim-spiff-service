package com.precorconnect.claimspiffservice.salesforce;

public interface SalesforceAdapterConfigFactory {

    SalesforceAdapterConfig construct();

}
