package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.UsernameImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public class ListSpiffLogsWithIdFeatureImpl implements
		ListSpiffLogsWithIdFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    private final SalesforceAdapter salesforceAdapter;

    private final OktaAdapter oktaAdapter;

    /*
    constructors
     */
    @Inject
    public ListSpiffLogsWithIdFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final SalesforceAdapter salesforceAdapter,
            @NonNull final OktaAdapter oktaAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.salesforceAdapter =
                guardThat(
                        "salesforceAdapter",
                        salesforceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oktaAdapter =
                guardThat(
                        "oktaAdapter",
                        oktaAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId) throws AuthenticationException {

		Collection<SpiffLogView> spiffLogs = databaseAdapter
				.listSpiffLogsWithId(accountId);
		Collection<SpiffLogView> modifiedSpiffLogs = new ArrayList<SpiffLogView>();

		for (SpiffLogView spiffLog : spiffLogs) {
			String partnerName = salesforceAdapter
									.getAccountNameWithId(spiffLog.getPartnerAccountId());
			String partnerRepName = oktaAdapter
										.getPartnerRepNameWithId(spiffLog.getPartnerRepUserId());

			spiffLog.setAccountName(new AccountNameImpl(partnerName));
			spiffLog.setUserName(new UsernameImpl(partnerRepName));

			modifiedSpiffLogs.add(spiffLog);
		}

		return modifiedSpiffLogs;

	}

}
