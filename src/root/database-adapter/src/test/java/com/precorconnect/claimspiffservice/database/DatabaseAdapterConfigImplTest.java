package com.precorconnect.claimspiffservice.database;

import com.precorconnect.Password;
import com.precorconnect.Username;
import org.junit.Test;

import java.net.URI;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertSame;

public class DatabaseAdapterConfigImplTest {

    /*
    fields
     */
    private final Dummy dummy = new Dummy();
    
    /*
    test methods
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_whenUriNull_shouldThrowIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                null,
                dummy.getUsername(),
                dummy.getPassword()
        );

    }

    @Test
    public void testConstructor_whenPassDummyUri_shouldSetDummyUri(
    ) throws Exception {
        
        /*
        arrange
         */

        URI expectedUri = dummy.getUri();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        expectedUri,
                        dummy.getUsername(),
                        dummy.getPassword()
                );
        
        /*
        act
         */
        
        URI actualUri =
                objectUnderTest.getUri();
        
        /*
        assert
         */

        assertSame(expectedUri, actualUri);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_whenUsernameNull_shouldThrowIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                null,
                dummy.getPassword()
        );

    }

    @Test
    public void testConstructor_whenPassDummyUsername_shouldSetThatUsername(
    ) throws Exception {
        
        /*
        arrange
         */

        Username expectedUsername = dummy.getUsername();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        expectedUsername,
                        dummy.getPassword()
                );
        
        
        /*
        act
         */

        Username actualUsername =
                objectUnderTest.getUsername();
        
        /*
        assert
         */

        assertThat(expectedUsername)
                .isSameAs(actualUsername);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_whenPasswordNull_shouldThrowsIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                dummy.getUsername(),
                null
        );

    }

    @Test
    public void testConstructor_whenPassDummyPassword_shouldSetThatPassword(
    ) throws Exception {
        
        /*
        arrange
         */

        Password expectedPassword = dummy.getPassword();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        dummy.getUsername(),
                        expectedPassword
                );
        
        
        /*
        act
         */

        Password actualPassword =
                objectUnderTest.getPassword();
        
        /*
        assert
         */

        assertThat(expectedPassword)
                .isSameAs(actualPassword);
    }

}