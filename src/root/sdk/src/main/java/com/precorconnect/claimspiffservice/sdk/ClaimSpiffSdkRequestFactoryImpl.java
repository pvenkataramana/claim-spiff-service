package com.precorconnect.claimspiffservice.sdk;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;



@Singleton
class ClaimSpiffSdkRequestFactoryImpl implements
        ClaimSpiffSdkRequestFactory {

    @Override
    public ClaimSpiffWebDto construct(
            @NonNull ClaimSpiffDto claimSpiffDto
    ) {

    	Long spiffEntitlementId =
        		claimSpiffDto
                        .getSpiffEntitlementId()
                        .getValue();

    	Long partnerSaleRegistrationId =
                		claimSpiffDto
                                .getPartnerSaleRegistrationId()
                                .getValue();


    	String partnerAccountId =
                		claimSpiffDto
                				.getPartnerAccountId()
                				.getValue();


    	String partnerRepUserId =
                		claimSpiffDto
                                .getPartnerRepUserId()
                                .getValue();

    	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    	Date sellDate = claimSpiffDto.getSellDate().getValue();

    	String sellDateString = dateFormat.format(sellDate);

    	Date installDate = claimSpiffDto.getInstallDate().getValue();

    	String installDateString = dateFormat.format(installDate);


    	Double spiffAmount =
                		claimSpiffDto
                                .getSpiffAmount()
                                .getValue();

    	String spiffClaimedDate = new Timestamp(System.currentTimeMillis()).toString();


    	String facilityName =
                		claimSpiffDto
                                .getFacilityName()
                                .getValue();

    	String invoiceNumber =
                		claimSpiffDto
                                .getInvoiceNumber()
                                .getValue();

        return
        		new ClaimSpiffWebDto(
        				spiffEntitlementId,
        				partnerSaleRegistrationId,
        				partnerAccountId,
        				partnerRepUserId,
        				sellDateString,
        				installDateString,
        				spiffAmount,
        				spiffClaimedDate,
        				facilityName,
        				invoiceNumber
                );
    }

}