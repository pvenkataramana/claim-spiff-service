package com.precorconnect.claimspiffservice.database;

import java.util.Collection;
import java.util.List;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;

public interface CreateSpiffEntitlementFeature {

	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			List<SpiffEntitlementDto> spiffEntitlements
			);

}
