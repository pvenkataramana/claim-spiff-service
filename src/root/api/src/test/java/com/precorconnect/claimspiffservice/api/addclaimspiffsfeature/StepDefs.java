package com.precorconnect.claimspiffservice.api.addclaimspiffsfeature;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.List;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceImpl;
import com.precorconnect.claimspiffservice.api.Config;
import com.precorconnect.claimspiffservice.api.ConfigFactory;
import com.precorconnect.claimspiffservice.api.Dummy;
import com.precorconnect.claimspiffservice.api.Factory;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	/*
	 fields
	 */
	private final Config config = new ConfigFactory()
										.construct();

	private final Dummy dummy = new Dummy();

	private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

	List<ClaimSpiffDto> claimSpiffDtoList ;

	private OAuth2AccessToken accessToken;

	private Collection<ClaimSpiffId> claimSpiffIdList;


	@Given("^a claimSpiffDtoList consists of:$")
	public void aclaimSpiffDtoListconsistsof(
			DataTable arg1
			) throws Throwable {
		// no op, we are creating spiffentitlementdtolist in dummy
	}

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
    ) throws Throwable {

        accessToken = new OAuth2AccessTokenImpl(
                								factory
                									.constructValidPartnerRepOAuth2AccessToken()
                									.getValue()
                								);

    }

    @Given("^provide a valid claimSpiffDtoList$")
    public void provideavalidclaimSpiffDtoList(
    ) throws Throwable {

    	claimSpiffDtoList =
                	dummy.getClaimSpiffDtoList();

    }

    @When("^I execute addCliamSpiffs$")
    public void IExecuteaddCliamSpiffs(
    ) throws Throwable {

        ClaimSpiffService objectUnderTest =
                					new ClaimSpiffServiceImpl(
                							config.getClaimSpiffServiceConfig()
                							);

        claimSpiffIdList = objectUnderTest
                						.addCliamSpiffs(
                								claimSpiffDtoList,
                								accessToken
                								);

    }

    @Then("^claimSpiffDtoList is added to the claimspiffs database with attributes:$")
    public void anspiffentitlementisaddedtothespiffentitlementdatabase(
    		DataTable arg
    ) throws Throwable {

    	assertEquals(
				dummy.getListClaimSpiffId().size(),
				claimSpiffIdList.size()
				);

    }


}
