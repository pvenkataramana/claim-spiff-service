package com.precorconnect.claimspiffservice.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceConfigFactoryImpl;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceImpl;


@Configuration
public class ClaimSpiffServiceBean {

    @Bean
    @Singleton
    public ClaimSpiffService claimSpiffService() {

        return
                new ClaimSpiffServiceImpl(
                		new ClaimSpiffServiceConfigFactoryImpl()
                			                                   .construct()
                );

    }
}
