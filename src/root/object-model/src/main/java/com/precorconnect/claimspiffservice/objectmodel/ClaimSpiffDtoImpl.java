package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;


public class ClaimSpiffDtoImpl implements ClaimSpiffDto {

    /*
    fields
    */
	private final SpiffEntitlementId spiffEntitlementId;

    private final PartnerSaleRegistrationId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

    private final SellDate sellDate;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SpiffClaimedDate spiffClaimedDate;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;


    /*
    constructors
    */
    public ClaimSpiffDtoImpl(
    		@NonNull final SpiffEntitlementId spiffEntitlementId,
            @NonNull final PartnerSaleRegistrationId partnerSaleRegistrationId,
            @NonNull final AccountId partnerAccountId,
            @NonNull final UserId partnerRepUserId,
            @NonNull final SellDate sellDate,
            @NonNull final InstallDate installDate,
            @NonNull final SpiffAmount spiffAmount,
            @Nullable final SpiffClaimedDate spiffClaimedDate,
            @NonNull final FacilityName facilityName,
            @NonNull final InvoiceNumber invoiceNumber
    ) {

    	this.spiffEntitlementId =
                guardThat(
                        "spiffEntitlementId",
                        spiffEntitlementId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                        partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.installDate =
                guardThat(
                        "installDate",
                         installDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffClaimedDate = spiffClaimedDate;


    	this.facilityName=
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();


    }

    /*
    getter & setter methods
    */
    public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

    public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public SellDate getSellDate() {

		return sellDate;
	}


}
