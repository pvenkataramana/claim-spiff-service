package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;

@Singleton
public class ClaimSpiffResponseFactoryImpl implements ClaimSpiffResponseFactory {

	@Override
	public ClaimSpiffView construct(
			 ClaimSpiff claimSpiff) {

		 guardThat(
                 "claimSpiff",
                 claimSpiff
               )
                .isNotNull();

		ClaimSpiffId claimId = new ClaimSpiffIdImpl(
				claimSpiff
				.getClaimId()
				.longValue()
				);

		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(
				claimSpiff
					.getPartnerSaleRegistrationId()
				);

		 AccountId partnerAccountId = new AccountIdImpl(
				 claimSpiff
				 .getPartnerAccountId()
				 );

		 UserId partnerRepUserId = new UserIdImpl(
				 claimSpiff
				 .getPartnerRepUserId()
				 );

		 InstallDate installDate =  new InstallDateImpl(
				 claimSpiff
				 .getInstallDate()
				 );

		 SpiffAmount spiffAmount = new SpiffAmountImpl(
				 claimSpiff
				 .getSpiffAmount()
				 );

		 SpiffClaimedDate spiffClaimedDate = new SpiffClaimedDateImpl(
				 claimSpiff
				 .getSpiffClaimedDate()
				 );

		 FacilityName facilityName = new FacilityNameImpl(
				 claimSpiff
				 .getFacilityName()
				 );

		 InvoiceNumber invoiceNumber = new InvoiceNumberImpl(
				 claimSpiff
				 .getInvoiceNumber()
				 );

		 SellDate sellDate = new SellDateImpl(
				 claimSpiff
				 .getSellDate()
				 );

		return
				new ClaimSpiffViewImpl(
				claimId,
				partnerSaleRegistrationId,
				partnerAccountId,
				partnerRepUserId,
				installDate,
				spiffAmount,
				spiffClaimedDate,
				facilityName,
				invoiceNumber,
				sellDate
				);
	}

}
