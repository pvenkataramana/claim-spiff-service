package com.precorconnect.claimspiffservice.objectmodel;

public interface FacilityName {

	String getValue();
}
