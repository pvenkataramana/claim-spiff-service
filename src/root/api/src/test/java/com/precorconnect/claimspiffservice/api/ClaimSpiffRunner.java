package com.precorconnect.claimspiffservice.api;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		 // features = {"features/AddClaimSpiffs.feature"},
		 features = {"features/GetClaimSpiffsWithIds.feature"},
	        glue = {"com.precorconnect.claimspiffserviceFeatures.api"}				
		)
public class ClaimSpiffRunner {
}