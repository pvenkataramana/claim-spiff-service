package com.precorconnect.claimspiffservice.webapi.updateinvoiceurlfeature;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.claimspiffservice.webapi.Config;
import com.precorconnect.claimspiffservice.webapi.ConfigFactory;
import com.precorconnect.claimspiffservice.webapi.Dummy;
import com.precorconnect.claimspiffservice.webapi.Factory;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs
		extends AbstractSpringIntegrationTest{
    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    private Long partnerSaleRegistrationId;

    private String invoiceUrl;

    private OAuth2AccessToken oAuth2AccessToken;

    private Response responseOfPostToSpiffEntitlements;

    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
    @Given("^a partnerSaleRegistrationId and invoiceUrl consists of:$")
    public void anaccountIdconsistsof(
            DataTable attributes
    ) throws Throwable {

        // no op , input prepared in dummy

    }

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideanaccessTokenidentifyingmeasapartnerrep(
    ) throws Throwable {

        oAuth2AccessToken =
                factory.constructValidPartnerRepOAuth2AccessToken();

    }

   @And("^provide a valid partnerSaleRegistrationId and invoiceUrl$")
   public void provideavalidpartnerSaleRegistrationIdandinvoiceUrl(
		   )throws Throwable{

	   partnerSaleRegistrationId =
			   dummy
			   .getSpiffEntitlementWebDto()
			   .getPartnerSaleRegistrationId();

	   invoiceUrl =
			   dummy
			   .getNewInvoiceUrl();
   }

   @When("^I execute updateInvoiceUrl$")
   public void IpostarequesttoupdateInvoiceUrl(
		   )throws Throwable{

	   responseOfPostToSpiffEntitlements =
   			given()
               .contentType(ContentType.JSON)
               .header(
                       "Authorization",
                       String.format(
                               "Bearer %s",
                               oAuth2AccessToken
                                       .getValue()
                       )
               )
               .body(invoiceUrl)
               .post("/claim-spiffs/entitlements/"
            		   +partnerSaleRegistrationId
               		   +"/invoiceurl"
               		   );
   }

   @Then("^the invoiceUrl of the matched partnerSaleRegistrationId are modified$")
   public void theinvoiceUrlofthematchedpartnerSaleRegistrationIdaremodified(
		   )throws Throwable{

	   responseOfPostToSpiffEntitlements
							.then()
							.assertThat()
							.statusCode(200);
   }


}
