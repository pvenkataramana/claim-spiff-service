package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;


public interface SpiffEntitlementsRequestFactory {

	SpiffEntitlement construct(
			@NonNull SpiffEntitlementDto spiffEntitlement
			);
}
