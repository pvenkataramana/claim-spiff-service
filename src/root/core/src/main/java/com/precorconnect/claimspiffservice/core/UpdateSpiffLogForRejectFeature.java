package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;

public interface UpdateSpiffLogForRejectFeature {

	void updateSpiffLogForReject(
			@NonNull Long claimId,
			@NonNull String reviewedBy
			) throws AuthenticationException, AuthorizationException;

}
