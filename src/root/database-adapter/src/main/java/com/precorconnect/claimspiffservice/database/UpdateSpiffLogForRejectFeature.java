package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateSpiffLogForRejectFeature {

	public void updateSpiffLogForReject(
			@NonNull Long claimId,
			@NonNull String reviewedBy
			);

}
