DROP TABLE submittedclaimspiff;

CREATE TABLE `spifflog` (
  `claimId` bigint(30) NOT NULL AUTO_INCREMENT,
  `partnerSaleRegistrationId` bigint(30) NOT NULL,
  `partnerAccountId` varchar(100) NOT NULL,
  `partnerRepUserId` varchar(100) NOT NULL,
  `sellDate` date NOT NULL,
  `installDate` date NOT NULL,
  `spiffAmount` double NOT NULL,
  `spiffClaimedDate` datetime NOT NULL,
  `facilityName` varchar(100) NOT NULL,
  `invoiceNumber` varchar(100) NOT NULL,
  `reviewStatus` varchar(100) DEFAULT NULL,
  `reviewedBy` varchar(100) DEFAULT NULL,
  `reviewedDate` date DEFAULT NULL,
  PRIMARY KEY (`claimId`)
);