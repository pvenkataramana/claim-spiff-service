package com.precorconnect.claimspiffservice.objectmodel;

import java.util.Date;

public interface SellDate {

	Date getValue();

}
