package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;

@Singleton
public class ListSpiffEntitlementFeatureImpl implements
		ListSpiffEntitlementFeature {

    /*
    fields
     */
    private final WebTarget spiffEntitlementsWebTarget;

    private final SpiffEntitlementsSdkResponseFactory spiffEntitlementsSdkResponseFactory;


    /*
    constructors
     */
    @Inject
    public ListSpiffEntitlementFeatureImpl(
            @NonNull final WebTarget spiffEntitlementsWebTarget,
            @NonNull final SpiffEntitlementsSdkResponseFactory spiffEntitlementsSdkResponseFactory
    ) {

    	this.spiffEntitlementsWebTarget =
                guardThat(
                        "spiffEntitlementsWebTarget",
                        spiffEntitlementsWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/claim-spiffs");

    	this.spiffEntitlementsSdkResponseFactory =
                guardThat(
                        "spiffEntitlementsSdkResponseFactory",
                        spiffEntitlementsSdkResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException {


		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        oAuth2AccessToken.getValue()
                );

		Collection<SpiffEntitlementWebView> SpiffEntitlementWebView;
        try {

        	SpiffEntitlementWebView =
            		spiffEntitlementsWebTarget
            				.path("/id/"+accountId.getValue())
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .get(
                                    new GenericType<
                                            Collection<SpiffEntitlementWebView>>() {
                                    }
                            );

        } catch (NotAuthorizedException e) {

            throw new AuthenticationException(e);

        }catch (Exception e) {

            throw new RuntimeException(e);

        }

        return
        		SpiffEntitlementWebView
        			.stream()
        			.map(spiffEntitlementsSdkResponseFactory::construct)
    			    .collect(Collectors.toList());

    }
}
