package com.precorconnect.claimspiffservice.database;

import com.precorconnect.Password;
import com.precorconnect.Username;

import java.net.URI;

public interface DatabaseAdapterConfig {

    URI getUri();

    Username getUsername();

    Password getPassword();

}
