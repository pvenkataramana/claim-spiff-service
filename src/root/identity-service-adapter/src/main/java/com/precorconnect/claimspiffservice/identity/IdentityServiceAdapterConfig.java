package com.precorconnect.claimspiffservice.identity;

import java.net.URL;

public interface IdentityServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
