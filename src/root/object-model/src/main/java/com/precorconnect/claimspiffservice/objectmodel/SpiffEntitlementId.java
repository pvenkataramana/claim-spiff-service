package com.precorconnect.claimspiffservice.objectmodel;

public interface SpiffEntitlementId {

	public Long getValue();

}
