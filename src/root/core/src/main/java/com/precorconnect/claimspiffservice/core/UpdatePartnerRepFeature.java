package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdatePartnerRepFeature {

	 void updatePartnerRep(
			 @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			 @NonNull UserId partnerRepUserId
			) throws AuthenticationException, AuthorizationException;

}
