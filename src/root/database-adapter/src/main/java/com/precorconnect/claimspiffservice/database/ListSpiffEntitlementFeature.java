package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;

public interface ListSpiffEntitlementFeature {

	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId);

}
