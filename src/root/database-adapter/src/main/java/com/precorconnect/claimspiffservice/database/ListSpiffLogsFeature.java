package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface ListSpiffLogsFeature {

	public Collection<SpiffLogView> listSpiffLogs();

}
