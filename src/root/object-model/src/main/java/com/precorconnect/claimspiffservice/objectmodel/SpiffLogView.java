package com.precorconnect.claimspiffservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;
import com.precorconnect.UserId;
import com.precorconnect.Username;

public interface SpiffLogView {
	/*
    fields
     */

	  ClaimSpiffId getClaimId();

      PartnerSaleRegistrationId getPartnerSaleRegistrationId();

      AccountId getPartnerAccountId();

      UserId getPartnerRepUserId();

      SellDate getSellDate();

      InstallDate getInstallDate();

      SpiffAmount getSpiffAmount();

      SpiffClaimedDate getSpiffClaimedDate();

      FacilityName getFacilityName();

      InvoiceNumber getInvoiceNumber();

      ReviewStatus getReviewStatus();

      ReviewedBy getReviewedBy();

  	  ReviewedDate getReviewedDate();

  	  AccountName getAccountName();

  	  Username getUserName();

  	  void setAccountName(AccountName accountName);

  	  void setUserName(Username userName);
}
