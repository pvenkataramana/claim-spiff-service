package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdatePartnerRepFeature {

	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId delaerRepUserid);

}
