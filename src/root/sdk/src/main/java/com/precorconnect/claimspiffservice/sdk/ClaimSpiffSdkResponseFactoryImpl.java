package com.precorconnect.claimspiffservice.sdk;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;

@Singleton
public class ClaimSpiffSdkResponseFactoryImpl implements ClaimSpiffSdkResponseFactory {

	@Override
	public ClaimSpiffView construct(@NonNull ClaimSpiffWebView claimSpiff) {

		ClaimSpiffId claimId = new ClaimSpiffIdImpl(
				claimSpiff
				.getClaimId()
				);

		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(
					claimSpiff
					.getPartnerSaleRegistrationId()
				);

		 AccountId partnerAccountId = new AccountIdImpl(
				 claimSpiff
				 .getPartnerAccountId()
				 );

		 UserId partnerRepUserId = new UserIdImpl(
				 claimSpiff
				 .getPartnerRepUserId()
				 );

		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date_install;
			try {
				date_install = dateFormat.parse(
						claimSpiff
								.getInstallDate());
				} catch (ParseException e) {
					throw new RuntimeException("install date parse exception: ", e);
				}

		 InstallDate installDate = new InstallDateImpl(date_install);

		 SpiffAmount spiffAmount = new SpiffAmountImpl(
				 claimSpiff
				 .getSpiffAmount()
				 );

		 SpiffClaimedDate spiffClaimedDate = new SpiffClaimedDateImpl(
				 new Timestamp(new java.util.Date().getTime())
				 );


		 FacilityName facilityName = new FacilityNameImpl(
				 claimSpiff
				 .getFacilityName()
				 );

		 InvoiceNumber invoiceNumber = new InvoiceNumberImpl(
				 claimSpiff
				 .getInvoiceNumber()
				 );

		 Date date_sell;
			try {
				date_sell = dateFormat.parse(
						claimSpiff
								.getSellDate());
				} catch (ParseException e) {
					throw new RuntimeException("sell date parse exception: ", e);
				}

		 SellDate sellDate = new SellDateImpl(
				 							date_sell
				 							);


		//String claimedDateString = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(spiffClaimedDate.getTime());

		return new ClaimSpiffViewImpl(
				claimId,
				partnerSaleRegistrationId,
				partnerAccountId,
				partnerRepUserId,
				installDate,
				spiffAmount,
				spiffClaimedDate,
				facilityName,
				invoiceNumber,
				sellDate
				);
	}

}
