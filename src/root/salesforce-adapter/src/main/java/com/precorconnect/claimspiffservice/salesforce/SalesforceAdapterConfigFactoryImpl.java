package com.precorconnect.claimspiffservice.salesforce;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.OAuth2ClientCredentialsImpl;
import com.precorconnect.OAuth2ClientId;
import com.precorconnect.OAuth2ClientIdImpl;
import com.precorconnect.OAuth2ClientSecret;
import com.precorconnect.OAuth2ClientSecretImpl;
import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfigImpl;

/**
 * Constructs {@link SalesforceAdapterConfig} from
 * system properties
 */
public class SalesforceAdapterConfigFactoryImpl
        implements SalesforceAdapterConfigFactory {

    @Override
    public SalesforceAdapterConfig construct() {

        return
                new SalesforceAdapterConfigImpl(
                        constructBaseOAuth2Url(),
                        constructBaseInstanceUrl(),
                        constructUsername(),
                        constructPassword(),
                        new OAuth2ClientCredentialsImpl(
                                constructConnectedAppClientId(),
                                constructConnectedAppClientSecret()
                        ),
                        new OAuth2ClientCredentialsImpl(
                                constructPrecorConnectClientId(),
                                constructPrecorConnectClientSecret()
                        ),
                        new IdentityServiceSdkConfigImpl(
                        		constructPrecorConnectApiBaseUrl()
                        )
                );

    }

    URL constructBaseOAuth2Url() {

        String baseOAuth2Url = System.getenv("SALESFORCE_BASE_OAUTH2_URL");

        try {

            return new URL(baseOAuth2Url);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }

    URL constructBaseInstanceUrl() {

        String baseInstanceUrl = System.getenv("SALESFORCE_BASE_INSTANCE_URL");

        try {

            return new URL(baseInstanceUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }

    Username constructUsername() {

        String username = System.getenv("SALESFORCE_USERNAME");
        return new UsernameImpl(username);

    }

    Password constructPassword() {

        String password = System.getenv("SALESFORCE_PASSWORD");

        return new PasswordImpl(password);

    }

    OAuth2ClientSecret constructConnectedAppClientSecret() {

        String connectedAppClientSecret =
                System.getenv("SALESFORCE_CONNECTED_APP_CLIENT_SECRET");

        return new OAuth2ClientSecretImpl(connectedAppClientSecret);

    }

    OAuth2ClientId constructConnectedAppClientId() {

        String connectedAppClientId =
                System.getenv("SALESFORCE_CONNECTED_APP_CLIENT_ID");

        return new OAuth2ClientIdImpl(connectedAppClientId);

    }

    private OAuth2ClientId constructPrecorConnectClientId() {

        String precorConnectClientId = System.getenv("PRECOR_CONNECT_CLIENT_ID");

        return
                new OAuth2ClientIdImpl(
                        precorConnectClientId
                );

    }

    private OAuth2ClientSecret constructPrecorConnectClientSecret() {

        String precorConnectClientSecret = System.getenv("PRECOR_CONNECT_CLIENT_SECRET");

        return
                new OAuth2ClientSecretImpl(
                        precorConnectClientSecret
                );
    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
