package com.precorconnect.claimspiffservice.api.listentitlementswithpartneridfeature;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceImpl;
import com.precorconnect.claimspiffservice.api.Config;
import com.precorconnect.claimspiffservice.api.ConfigFactory;
import com.precorconnect.claimspiffservice.api.Dummy;
import com.precorconnect.claimspiffservice.api.Factory;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	/*
	 fields
	 */
	private final Config config = new ConfigFactory()
										.construct();

	private final Dummy dummy = new Dummy();

	private final Factory factory =
            					new Factory(
            								dummy,
            								new IdentityServiceIntegrationTestSdkImpl(
            											config
            												.getIdentityServiceJwtSigningKey()
            											)
            							);

	private OAuth2AccessToken accessToken;

	private AccountId accountId;

	private Collection<SpiffEntitlementView> spiffEntitlementViewList;

	@Given("^a accountId consists of:$")
	public void aaccountIdconsistsof(
			DataTable arg
			)throws Throwable{
		// no op, accountId is taken from dummy
	}

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
    ) throws Throwable {

        accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
												);

    }

    @Given("^provide a valid accountId$")
    public void provideAValidAccountId(
    ) throws Throwable {

    	accountId =
                	dummy.getPartnerAccountId();

    }

    @When("^I execute listEntitlementsWithPartnerId$")
    public void iExecuteListEntitlementsWithPartnerId(
    ) throws Throwable {

    	ClaimSpiffService objectUnderTest =
										new ClaimSpiffServiceImpl(
													config.getClaimSpiffServiceConfig()
													);

        spiffEntitlementViewList = objectUnderTest
                						.listEntitlementsWithPartnerId(
                								accountId,
                								accessToken
                								);

    }

    @Then("^the spiffentitlements with the matched accountid are returned$")
    public void thespiffentitlementswiththematchedaccountidarereturned(
    ) throws Throwable {

    	assertThat(spiffEntitlementViewList.size())
						.isGreaterThan(0);

    }

}
