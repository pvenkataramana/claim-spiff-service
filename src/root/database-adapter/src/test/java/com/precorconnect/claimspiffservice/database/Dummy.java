package com.precorconnect.claimspiffservice.database;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDate;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementViewImpl;


public class Dummy {

    /*
    fields
     */
    private URI uri;

    private Username username = new UsernameImpl("precoruser");

    private Password password = new PasswordImpl("precorpwd");

    private Collection<ClaimSpiffDto> claimSpiffDtoList = new ArrayList<ClaimSpiffDto>();

    private Collection<ClaimSpiffId> claimSpiffIds = new ArrayList<ClaimSpiffId>();

    private Long claimId=10L;

    private Long partnerSaleRegistrationId=12L;

    private String partnerAccountId="001K000001H2Km2IAF";

    private String partnerRepUserId="00u5i5aimx8ChKQvB0h7";

    private String  installDate="11/21/1999";

    private String sellDate = "11/21/1999";

    private Double spiffAmount = 100.00;

    private SpiffClaimedDate spiffClaimedDate= new SpiffClaimedDateImpl(new Timestamp(System.currentTimeMillis()));

    private Long facilityId=12L;

    private String facilityName="precor";

    private String invoiceNumber="11111";

    private List<ClaimSpiffDto> claimSpiffDtloList=new ArrayList<ClaimSpiffDto>();

    private Date date_install;

    private Date date_sell;

    private ClaimSpiff claimSpiff;

    private ClaimSpiffView claimSpiffView;

    private ClaimSpiffDto claimSpiffDto;

    private Long spiffEntitlementId = 1L;

    private SpiffEntitlementDto spiffEntitlementDto;

    private List<SpiffEntitlementDto> spiffEntitlementDtosList = new ArrayList<SpiffEntitlementDto>();

    private SpiffEntitlement spiffEntitlement;

    private SpiffEntitlementView spiffEntitlementView;

    private String newInvoiceUrl = "http://www.precor.com/test";

    private String newUserId = "222";

    private String invoiceUrl = "www.testinvoice.com";

    private List<SpiffEntitlementId> entitlementIds = new ArrayList<SpiffEntitlementId>();
    /*
    constructors
     */
    public Dummy() {

    	 try {

             uri = new URI("http://dev.precorconnect.com");

             try {
            	 date_install = new SimpleDateFormat("MM/dd/yyyy").parse(installDate);
 			} catch (ParseException e) {
 				throw new RuntimeException("date parsing exception: ", e);
 			}

             try {
            	 date_sell = new SimpleDateFormat("MM/dd/yyyy").parse(installDate);
 			} catch (ParseException e) {
 				throw new RuntimeException("date parsing exception: ", e);
 			}



         claimSpiffDto = new ClaimSpiffDtoImpl(
        		new SpiffEntitlementIdImpl(spiffEntitlementId),
        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
        		new AccountIdImpl(partnerAccountId),
        		new UserIdImpl(partnerRepUserId),
        		new SellDateImpl(date_sell),
        		new InstallDateImpl(date_install),
        		new SpiffAmountImpl(spiffAmount),
        		new SpiffClaimedDateImpl(spiffClaimedDate.getValue()),
        		new FacilityNameImpl(facilityName),
        		new InvoiceNumberImpl(invoiceNumber)
        		);

        claimSpiffDtoList.add(claimSpiffDto);

        claimSpiff=new ClaimSpiff();
        claimSpiff.setClaimId(claimId);
        claimSpiff.setPartnerSaleRegistrationId(partnerSaleRegistrationId);
        claimSpiff.setPartnerAccountId(partnerAccountId);
        claimSpiff.setPartnerRepUserId(partnerRepUserId);
        claimSpiff.setInstallDate(date_install);
        claimSpiff.setSpiffAmount(spiffAmount);
        claimSpiff.setSpiffClaimedDate(spiffClaimedDate.getValue());
        claimSpiff.setFacilityName(facilityName);
        claimSpiff.setInvoiceNumber(invoiceNumber);

        claimSpiffIds.add(new ClaimSpiffIdImpl(10L));

        claimSpiffView=new ClaimSpiffViewImpl(
        		new ClaimSpiffIdImpl(claimId),
        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
        		new AccountIdImpl(partnerAccountId),
        		new UserIdImpl(partnerRepUserId),
        		new InstallDateImpl(date_install),
        		new SpiffAmountImpl(spiffAmount),
        		new SpiffClaimedDateImpl(spiffClaimedDate.getValue()),
        		new FacilityNameImpl(facilityName),
        		new InvoiceNumberImpl(invoiceNumber),
        		new SellDateImpl(date_sell)
        		);


        spiffEntitlementDto = new SpiffEntitlementDtoImpl(
										new AccountIdImpl(partnerAccountId),
										new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
										new FacilityNameImpl(facilityName),
										new InvoiceNumberImpl(invoiceNumber),
										new InvoiceUrlImpl(invoiceUrl),
										new UserIdImpl(partnerRepUserId),
										new InstallDateImpl(date_install),
										new SpiffAmountImpl(spiffAmount),
										new SellDateImpl(date_sell)
        								);

        spiffEntitlementDtosList.add(spiffEntitlementDto);

        spiffEntitlement = new SpiffEntitlement();
        spiffEntitlement.setSpiffEntitlementId(spiffEntitlementId);
        spiffEntitlement.setAccountId(partnerAccountId);
        spiffEntitlement.setPartnerSaleRegistrationId(partnerSaleRegistrationId);
        spiffEntitlement.setFacilityName(facilityName);
        spiffEntitlement.setInvoiceNumber(invoiceNumber);
        spiffEntitlement.setInvoiceUrl(invoiceUrl);
        spiffEntitlement.setPartnerRepUserId(partnerRepUserId);
        spiffEntitlement.setInstallDate(date_install);
        spiffEntitlement.setSpiffAmount(spiffAmount);

        spiffEntitlementView = new SpiffEntitlementViewImpl(
        							new SpiffEntitlementIdImpl(spiffEntitlementId),
        							new AccountIdImpl(partnerAccountId),
        							new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
        							new FacilityNameImpl(facilityName),
        							new InvoiceNumberImpl(invoiceNumber),
        							new InvoiceUrlImpl(invoiceUrl),
        							new UserIdImpl(partnerRepUserId),
        							new InstallDateImpl(date_install),
        							new SpiffAmountImpl(spiffAmount),
        							new SellDateImpl(date_sell)
        							);

        entitlementIds.add(new SpiffEntitlementIdImpl(1L));

    	 } catch (URISyntaxException e) {

             throw new RuntimeException(e);

         }
    }

    /*
    getter methods
     */

	public Date getDate_install() {
		return date_install;
	}

	public void setDate_install(Date date_install) {
		this.date_install = date_install;
	}

	public Date getDate_sell() {
		return date_sell;
	}

	public void setDate_sell(Date date_sell) {
		this.date_sell = date_sell;
	}

	public URI getUri() {
		return uri;
	}

	public Username getUsername() {
		return username;
	}

	public Password getPassword() {
		return password;
	}

	public Collection<ClaimSpiffDto> getClaimSpiffDtoList() {
		return claimSpiffDtoList;
	}

	public Collection<ClaimSpiffId> getClaimSpiffIds() {
		return claimSpiffIds;
	}

	public Long getClaimId() {
		return claimId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public Long getFacilityId() {
		return facilityId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public List<ClaimSpiffDto> getClaimSpiffDtloList() {
		return claimSpiffDtloList;
	}

	public ClaimSpiff getClaimSpiff() {
		return claimSpiff;
	}

	public ClaimSpiffView getClaimSpiffView() {
		return claimSpiffView;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public void setUsername(Username username) {
		this.username = username;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	public void setClaimSpiffDtoList(Collection<ClaimSpiffDto> claimSpiffDtoList) {
		this.claimSpiffDtoList = claimSpiffDtoList;
	}

	public void setClaimSpiffIds(Collection<ClaimSpiffId> claimSpiffIds) {
		this.claimSpiffIds = claimSpiffIds;
	}

	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	public void setPartnerSaleRegistrationId(Long partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	public void setPartnerAccountId(String partnerAccountId) {
		this.partnerAccountId = partnerAccountId;
	}

	public void setPartnerRepUserId(String partnerRepUserId) {
		this.partnerRepUserId = partnerRepUserId;
	}

	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}

	public void setSpiffAmount(Double spiffAmount) {
		this.spiffAmount = spiffAmount;
	}

	public void setSpiffClaimedDate(SpiffClaimedDate spiffClaimedDate) {
		this.spiffClaimedDate = spiffClaimedDate;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public void setClaimSpiffDtloList(List<ClaimSpiffDto> claimSpiffDtloList) {
		this.claimSpiffDtloList = claimSpiffDtloList;
	}


	public void setClaimSpiff(ClaimSpiff claimSpiff) {
		this.claimSpiff = claimSpiff;
	}

	public void setClaimSpiffView(ClaimSpiffView claimSpiffView) {
		this.claimSpiffView = claimSpiffView;
	}

	public ClaimSpiffDto getClaimSpiffDto() {
		return claimSpiffDto;
	}

	public void setClaimSpiffDto(ClaimSpiffDto claimSpiffDto) {
		this.claimSpiffDto = claimSpiffDto;
	}

	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtosList() {
		return spiffEntitlementDtosList;
	}

	public SpiffEntitlement getSpiffEntitlement() {
		return spiffEntitlement;
	}

	public SpiffEntitlementView getSpiffEntitlementView() {
		return spiffEntitlementView;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public List<SpiffEntitlementId> getEntitlementIds() {
		return entitlementIds;
	}

}
