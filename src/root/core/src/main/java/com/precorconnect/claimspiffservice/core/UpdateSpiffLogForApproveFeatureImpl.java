package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;

@Singleton
public class UpdateSpiffLogForApproveFeatureImpl implements
		UpdateSpiffLogForApproveFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateSpiffLogForApproveFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updateSpiffLogForApprove(
			@NonNull Long claimId,
			@NonNull String reviewedBy
			) throws AuthenticationException,AuthorizationException {

		databaseAdapter
			.updateSpiffLogForApprove(
									claimId,
									reviewedBy
									);

	}

}
