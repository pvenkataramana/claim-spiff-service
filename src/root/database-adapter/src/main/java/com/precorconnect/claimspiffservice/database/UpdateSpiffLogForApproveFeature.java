package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateSpiffLogForApproveFeature {

	 public void updateSpiffLogForApprove(
				@NonNull Long claimId,
				@NonNull String reviewedBy
				);

}
