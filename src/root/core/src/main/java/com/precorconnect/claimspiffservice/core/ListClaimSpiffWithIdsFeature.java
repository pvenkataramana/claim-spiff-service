package com.precorconnect.claimspiffservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;

interface ListClaimSpiffWithIdsFeature {

    Collection<ClaimSpiffView> getClaimSpiffsWithIds(
    									@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds
    									) throws AuthenticationException;

}
