CREATE TABLE `submittedclaimspiff` (
  `claimId` BIGINT NOT NULL AUTO_INCREMENT,
  `partnerSaleRegistrationId` BIGINT(30) NULL,
  `partnerAccountId` BIGINT(30) NULL,
  `partnerRepUserId` BIGINT(30) NULL,
  `installDate` DATE NULL,
  `spiffAmount` DOUBLE NULL,
  `spiffClaimedDate` DATETIME NULL,
  `facilityId` BIGINT(30) NULL,
  `facilityName` VARCHAR(100) NULL,
  `invoiceNumber` VARCHAR(100) NULL,
  PRIMARY KEY (`claimId`))
;
