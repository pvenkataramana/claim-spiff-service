package com.precorconnect.claimspiffservice.api;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/AddClaimSpiffs.feature"},
        glue = {"com.precorconnect.claimspiffservice.api.addclaimspiffsfeature"}
        )
public class AddClaimSpiffsFeatureIT {

}
