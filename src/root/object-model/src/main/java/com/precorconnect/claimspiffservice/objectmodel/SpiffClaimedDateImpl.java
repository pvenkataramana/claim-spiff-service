package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffClaimedDateImpl implements SpiffClaimedDate{

	 /*
    fields
     */
    private final Timestamp value;
    
    /*
    constructor methods
     */
    public SpiffClaimedDateImpl(
    		@NonNull Timestamp value
    		){
    	
    	this.value =
                guardThat(
                        "spiffClaimedDate",
                         value
                )
                        .isNotNull()
                        .thenGetValue();

    }
    
    /*
    getter methods
     */
	@Override
	public Timestamp getValue() {

		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpiffClaimedDateImpl other = (SpiffClaimedDateImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	
}
