package com.precorconnect.claimspiffservice.core;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;

@RunWith(MockitoJUnitRunner.class)
public class CreateSpiffEntitlementFeatureImplTest {

	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private CreateSpiffEntitlementFeatureImpl createSpiffEntitlementFeatureImpl ;

	private Dummy dummy = new Dummy();

	@Test
	public void testCreateSpiffEntitlements_whenSpiffEntitlementDtoList_shownSpiffEntitlementIdList() throws AuthenticationException, AuthorizationException {

		when(databaseAdapter
				.createSpiffEntitlements(
						dummy.getSpiffEntitlementDtoList()
						)
			)
			.thenReturn(
						dummy.getSpiffEntitlementIdList()
						);

		Collection<SpiffEntitlementId> spiffEntitlementIdList = createSpiffEntitlementFeatureImpl
																		.createSpiffEntitlements(
																				dummy.getSpiffEntitlementDtoList()
																				);
		assertEquals(
				dummy.getSpiffEntitlementIdList().size(),
				spiffEntitlementIdList.size()
				);
	}

}
