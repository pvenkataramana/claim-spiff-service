
package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;


public class SpiffEntitlementDtoImpl
		implements SpiffEntitlementDto {

	/*
	 fields
	*/
    private final AccountId accountId;

    private final PartnerSaleRegistrationId partnerSaleRegistrationId;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;

    private final InvoiceUrl invoiceUrl;

    private final UserId partnerRepUserId;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SellDate sellDate;


    /*
    constructors
    */
    public SpiffEntitlementDtoImpl(
            @NonNull final AccountId accountId,
            @NonNull final PartnerSaleRegistrationId partnerSaleRegistrationId,
            @NonNull final FacilityName facilityName,
            @NonNull final InvoiceNumber invoiceNumber,
            final InvoiceUrl invoiceUrl,
            final UserId partnerRepUserId,
            @NonNull final InstallDate installDate,
            @NonNull final SpiffAmount spiffAmount,
            @NonNull final SellDate sellDate
    ) {

    	this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceUrl = invoiceUrl;

    	this.partnerRepUserId = partnerRepUserId;

    	this.installDate =
                guardThat(
                        "installDate",
                        installDate
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter & setter methods
    */
	@Override
	public final AccountId getAccountId() {

		return accountId;
	}

	@Override
	public final PartnerSaleRegistrationId getPartnerSaleRegistrationId() {

		return partnerSaleRegistrationId;
	}

	@Override
	public final FacilityName getFacilityName() {

		return facilityName;
	}

	@Override
	public final InvoiceNumber getInvoiceNumber() {

		return invoiceNumber;
	}

	@Override
	public final InvoiceUrl getInvoiceUrl() {

		return invoiceUrl;
	}

	@Override
	public final UserId getPartnerRepUserId() {

		return partnerRepUserId;
	}

	@Override
	public final InstallDate getInstallDate() {

		return installDate;
	}

	@Override
	public final SpiffAmount getSpiffAmount() {

		return spiffAmount;
	}

	@Override
	public final SellDate getSellDate() {

		return sellDate;
	}
}
