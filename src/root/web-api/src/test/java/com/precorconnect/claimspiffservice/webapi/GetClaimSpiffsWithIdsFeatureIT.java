package com.precorconnect.claimspiffservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/GetClaimSpiffsWithIds.feature"},
        glue = {"com.precorconnect.claimspiffservice.webapi.getclaimspiffswithidsfeature"}
        )
public class GetClaimSpiffsWithIdsFeatureIT {

}
