package com.precorconnect.claimspiffservice.sdk;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffClaimedDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private Collection<ClaimSpiffDto> claimSpiffDtoList = new ArrayList<ClaimSpiffDto>();

    private ClaimSpiffDto claimSpiffDto;

    private Collection<ClaimSpiffId> claimSpiffIds;

    private SpiffEntitlementDto spiffEntitlementDto;

    private SpiffEntitlementWebDto spiffEntitlementWebDto;

    private List<SpiffEntitlementDto> spiffEntitlementDtosList = new ArrayList<SpiffEntitlementDto>();

    private List<SpiffEntitlementWebDto> spiffEntitlementWebDtosList = new ArrayList<SpiffEntitlementWebDto>();

    private Date date;

    private String newInvoiceUrl;

    private String newUserId;

    private List<SpiffEntitlementId> entitlementIds = new ArrayList<SpiffEntitlementId>();
    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

        Date sellDate;
        try {
        	sellDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/24/2015");
		} catch (ParseException e) {
			throw new RuntimeException("date parsing exception: ", e);
		}

        Date installDate;
        try {
        	installDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/24/2015");
		} catch (ParseException e) {
			throw new RuntimeException("date parsing exception: ", e);
		}

         claimSpiffDto = new ClaimSpiffDtoImpl(
        		new SpiffEntitlementIdImpl(1L),
        		new PartnerSaleRegistrationIdImpl(123L),
        		new AccountIdImpl("001K000001H2Km2IAF"),
        		new UserIdImpl("00u5i5aimx8ChKQvB0h7"),
        		new SellDateImpl(sellDate),
        		new InstallDateImpl(installDate),
        		new SpiffAmountImpl(120.0d),
        		new SpiffClaimedDateImpl(new Timestamp(System.currentTimeMillis())),
        		new FacilityNameImpl("Precor"),
        		new InvoiceNumberImpl("11111")
        		);

         claimSpiffDtoList.add(claimSpiffDto);

        claimSpiffIds = new ArrayList<ClaimSpiffId>();
        claimSpiffIds.add(new ClaimSpiffIdImpl(1L));
        claimSpiffIds.add(new ClaimSpiffIdImpl(2L));

        try {
			date = new SimpleDateFormat("MM/dd/yyyy").parse("11/21/1999");
		} catch (ParseException e) {
			throw new RuntimeException("date parsing exception: ", e);
		}

        newInvoiceUrl = "http://www.yahoo.com/test";

        newUserId = "222";

        spiffEntitlementDto = new
        						SpiffEntitlementDtoImpl(
        								new AccountIdImpl("001K000001H2Km2IAF"),
        								new PartnerSaleRegistrationIdImpl(12L),
        								new FacilityNameImpl("precor"),
        								new InvoiceNumberImpl("1223112"),
        								new InvoiceUrlImpl("http://www.precortest.com"),
        								new UserIdImpl("00u5i5aimx8ChKQvB0h7"),
        								new InstallDateImpl(date),
        								new SpiffAmountImpl(100.00),
        								new SellDateImpl(date)
        								);

        spiffEntitlementDtosList.add(spiffEntitlementDto);

        spiffEntitlementWebDto = new SpiffEntitlementWebDto(
        												"001K000001H2Km2IAF",
        												12L,
        												"precor",
        												"112211",
        												"http://www.precortest.com",
        												"00u5i5aimx8ChKQvB0h7",
        												"11/21/1985",
        												100.00,
        												"11/21/1985"
        												);

        spiffEntitlementWebDtosList.add(spiffEntitlementWebDto);

        entitlementIds.add(new SpiffEntitlementIdImpl(1L));
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

	public Collection<ClaimSpiffDto> getClaimSpiffDtoList() {
		return claimSpiffDtoList;
	}

	public Collection<ClaimSpiffId> getClaimSpiffIds() {
		return claimSpiffIds;
	}

	public ClaimSpiffDto getClaimSpiffDto() {
		return claimSpiffDto;
	}

	public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public SpiffEntitlementWebDto getSpiffEntitlementWebDto() {
		return spiffEntitlementWebDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtosList() {
		return spiffEntitlementDtosList;
	}

	public List<SpiffEntitlementWebDto> getSpiffEntitlementWebDtosList() {
		return spiffEntitlementWebDtosList;
	}

	public Date getDate() {
		return date;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}

	public List<SpiffEntitlementId> getEntitlementIds() {
		return entitlementIds;
	}

}
