package com.precorconnect.claimspiffservice.api.updatepartnerrepfeature;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceImpl;
import com.precorconnect.claimspiffservice.api.Config;
import com.precorconnect.claimspiffservice.api.ConfigFactory;
import com.precorconnect.claimspiffservice.api.Dummy;
import com.precorconnect.claimspiffservice.api.Factory;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	/*
	 fields
	 */
	private final Config config = new ConfigFactory()
										.construct();

	private final Dummy dummy = new Dummy();

	private final Factory factory =
			new Factory(
						dummy,
						new IdentityServiceIntegrationTestSdkImpl(
									config
										.getIdentityServiceJwtSigningKey()
									)
					);

	private OAuth2AccessToken accessToken;

	private PartnerSaleRegistrationId partnerSaleRegistrationId;

	private UserId userId;

	@Given("^a partnerSaleRegistrationId and invoiceUrl consists of:$")
	public void apartnerSaleRegistrationIdandinvoiceUrlconsistsof(
			DataTable arg
			)throws Throwable{
		// no op, inputs are taken from dummy
	}

   @Given("^I provide an accessToken identifying me as a partner rep$")
   public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
   ) throws Throwable {

       accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
											);

   }

   @Given("^provide a valid partnerSaleRegistrationId and userId$")
   public void provideAValidPartnerSaleRegistrationId(
   ) throws Throwable {

   		partnerSaleRegistrationId =
               		dummy.getPartnerSaleRegistrationId();

   		userId =
   				dummy.getNewPartnerRepUserId();

   }

   @When("^I execute updatePartnerRep$")
   public void iExecuteUpdatePartnerRep(
   ) throws Throwable {

	   ClaimSpiffService objectUnderTest =
								new ClaimSpiffServiceImpl(
											config.getClaimSpiffServiceConfig()
											);

       objectUnderTest
               	.updatePartnerRep(
               			partnerSaleRegistrationId,
               			userId,
               			accessToken
               			);

   }

   @Then("^the userId of the matched partnerSaleRegistrationId are modified$")
   public void theuseridofthematchedpartnersaleregistrationidaremodified(
   ) throws Throwable {

	 //nothing to test for void method

   }
}
