package com.precorconnect.claimspiffservice.okta;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2ClientCredentials;

public class OktaAdapterConfigImpl
        implements OktaAdapterConfig {

    /*
    fields
     */
    private final URL oktaBaseUrl;

    private final OktaApiToken oktaApiToken;

    private final OAuth2ClientCredentials precorConnectClientCredentials;


    /*
    constructors
     */
    public OktaAdapterConfigImpl(
            @NonNull URL oktaBaseUrl,
            @NonNull OktaApiToken oktaApiToken,
            @NonNull OAuth2ClientCredentials precorConnectClientCredentials
    ) {

    	this.oktaBaseUrl =
                guardThat(
                        "oktaBaseUrl",
                         oktaBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oktaApiToken =
                guardThat(
                        "oktaApiToken",
                         oktaApiToken
                )
                        .isNotNull()
                        .thenGetValue();

    	this.precorConnectClientCredentials =
                guardThat(
                        "precorConnectClientCredentials",
                         precorConnectClientCredentials
                )
                        .isNotNull()
                        .thenGetValue();


    }

    /*
    getter methods
     */
    @Override
    public URL getOktaBaseUrl() {
        return oktaBaseUrl;
    }

    @Override
    public OktaApiToken getOktaApiToken() {
        return oktaApiToken;
    }

    @Override
    public OAuth2ClientCredentials getPrecorConnectClientCredentials() {
        return precorConnectClientCredentials;
    }


    /*
    equality methods
    */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((oktaApiToken == null) ? 0 : oktaApiToken.hashCode());
		result = prime * result
				+ ((oktaBaseUrl == null) ? 0 : oktaBaseUrl.hashCode());
		result = prime
				* result
				+ ((precorConnectClientCredentials == null) ? 0
						: precorConnectClientCredentials.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OktaAdapterConfigImpl other = (OktaAdapterConfigImpl) obj;
		if (oktaApiToken == null) {
			if (other.oktaApiToken != null) {
				return false;
			}
		} else if (!oktaApiToken.equals(other.oktaApiToken)) {
			return false;
		}
		if (oktaBaseUrl == null) {
			if (other.oktaBaseUrl != null) {
				return false;
			}
		} else if (!oktaBaseUrl.equals(other.oktaBaseUrl)) {
			return false;
		}
		if (precorConnectClientCredentials == null) {
			if (other.precorConnectClientCredentials != null) {
				return false;
			}
		} else if (!precorConnectClientCredentials
				.equals(other.precorConnectClientCredentials)) {
			return false;
		}
		return true;
	}

}
