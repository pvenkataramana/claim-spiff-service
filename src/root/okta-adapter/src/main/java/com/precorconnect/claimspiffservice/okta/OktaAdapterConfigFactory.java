package com.precorconnect.claimspiffservice.okta;

public interface OktaAdapterConfigFactory {

    OktaAdapterConfig construct();

}
