package com.precorconnect.claimspiffservice.database;


import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.core.DatabaseAdapter;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseAdapterImplIT {

    /*
    fields
     */
    private final Config config =
    		new ConfigFactory()
    		.construct();

    private final Dummy dummy =
    		new Dummy();

    /*
    test methods
     */
    @Test
    public void test1CreateSpiffEntitlements_whenEntitlementDtoList_shouldReturnsEntitlementIds(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        Collection<SpiffEntitlementId>
                entitlementIds =
                				objectUnderTest
                				.createSpiffEntitlements(
                						dummy.getSpiffEntitlementDtosList()
                						);

        assertThat(entitlementIds.size())
                .isGreaterThan(0);
    }

    @Test
    public void test2ListEntitlementsWithPartnerId_whenAccountId_shouldReturnsListOfEntitlements(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        Collection<SpiffEntitlementView>
                listEntitlements =
                			objectUnderTest
                			.listEntitlementsWithPartnerId(
                					dummy
                						.getSpiffEntitlementDto()
                						.getAccountId()
                					);

        assertThat(listEntitlements.size())
                .isGreaterThan(0);
    }

    @Test
    public void tes3tUpdateInvoiceUrl_whenNewInvoiceUrl_shouldReturnsNothing(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        objectUnderTest
                	.updateInvoiceUrl(
                			dummy
                				.getSpiffEntitlementDto()
                				.getPartnerSaleRegistrationId(),
                			new InvoiceUrlImpl(
                					dummy.getNewInvoiceUrl()
                					)
                			);
    }

    @Test
    public void test4UpdatePartnerRep_whenPartnerRepUserId_shouldReturnsNothing(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        objectUnderTest.updatePartnerRep(
        		dummy
        			.getSpiffEntitlementDto()
        			.getPartnerSaleRegistrationId(),
        		new UserIdImpl(
        				dummy.getNewUserId()
        				)
        		);

    }

    @Test
    public void test5addCliamSpiffs_whenClaimSpiffDtoList_shouldReturnsClaimSpiffIds(
    ) throws Exception {

        /*
        arrange
         */
    	try{
        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        /*
        act
         */
        Collection<ClaimSpiffId> ids =
        		objectUnderTest
        		.addClaimSpiffs(
        				dummy
        				.getClaimSpiffDtoList()
        				);

        /*
        assert
         */
        assertThat(ids.size())
                .isGreaterThan(0);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }

    @Test
    public void test6getClaimSpiffsWithIds_whenClaimSpiffIds_shouldReturnsListOfClaimSpiffs(
    ) throws Exception {

        /*
        arrange
         */
    	try{
        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()

                );

        /*
        act
         */
        Collection<ClaimSpiffView>
                claimSpiffIds =
                objectUnderTest
                .getClaimSpiffsWithIds(
                		dummy
                		.getClaimSpiffIds()
                		);

        /*
        assert
         */
        assertThat(claimSpiffIds.size())
                .isGreaterThanOrEqualTo(0);

		}catch(Exception e){
			e.printStackTrace();
		}
    }

}
