package com.precorconnect.claimspiffservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface ListSpiffLogsWithIdFeature {

	public Collection<SpiffLogView> listSpiffLogsWithId(
												@NonNull AccountId accountId
												);

}
