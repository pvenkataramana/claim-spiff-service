package com.precorconnect.claimspiffservice.salesforce;

import com.precorconnect.OAuth2AccessToken;

interface OAuth2AccessTokenFactory {

    OAuth2AccessToken construct(
    );

}
