package com.precorconnect.claimspiffservice.sdk;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

public interface AddClaimSpiffFeature {

	Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;
}
