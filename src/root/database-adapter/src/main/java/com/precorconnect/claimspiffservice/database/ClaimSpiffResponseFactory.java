package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;

public interface ClaimSpiffResponseFactory {
	
	ClaimSpiffView construct(
			@NonNull ClaimSpiff claimSpiff
			);
	

}
