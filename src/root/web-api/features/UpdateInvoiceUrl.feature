Feature: Update InvoiceUrl
  Update InvoiceUrl

 Background:
    Given a partnerSaleRegistrationId and invoiceUrl consists of:
      | attribute     				| validation | type   |
      | partnerSaleRegistrationId   | required   | number |
      | invoiceUrl                  | required   | string | 

  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid partnerSaleRegistrationId and invoiceUrl
    When I execute updateInvoiceUrl
    Then the invoiceUrl of the matched partnerSaleRegistrationId are modified