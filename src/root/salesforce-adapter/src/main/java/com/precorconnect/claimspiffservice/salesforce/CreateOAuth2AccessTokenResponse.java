package com.precorconnect.claimspiffservice.salesforce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
class CreateOAuth2AccessTokenResponse {

    /*
    fields
     */
    private String access_token;

    /*
    getter & setter methods
     */
    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateOAuth2AccessTokenResponse that = (CreateOAuth2AccessTokenResponse) o;

        return !(access_token != null ? !access_token.equals(that.access_token) : that.access_token != null);

    }

    @Override
    public int hashCode() {
        return access_token != null ? access_token.hashCode() : 0;
    }
}
