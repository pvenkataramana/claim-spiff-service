package com.precorconnect.claimspiffservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;

public class ClaimSpiffServiceSdkConfigFactoryImpl 
		implements ClaimSpiffServiceSdkConfigFactory {

	@Override
	public ClaimSpiffServiceSdkConfig construct() {

		return 
				new ClaimSpiffServiceSdkConfigImpl(
						constructprecorConnectApiBaseUrl()
				);
	}

	public URL constructprecorConnectApiBaseUrl(){

		try {

            String precorConnectApiBaseUrl=System
            		            .getenv("PRECOR_CONNECT_API_BASE_URL");

            return
            		new URL(
            		  precorConnectApiBaseUrl
            		);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }
}
	
}
