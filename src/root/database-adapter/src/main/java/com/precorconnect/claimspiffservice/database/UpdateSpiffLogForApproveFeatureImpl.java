package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

@Singleton
public class UpdateSpiffLogForApproveFeatureImpl implements UpdateSpiffLogForApproveFeature{

    private final SessionFactory sessionFactory;


    @Inject
    public UpdateSpiffLogForApproveFeatureImpl(
            @NonNull final SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

	@Override
	public void updateSpiffLogForApprove(
			@NonNull Long claimId,
			@NonNull String reviewedBy
			) {

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {

        	Query query= session
            		.createQuery("update ClaimSpiff set reviewedBy= :reviewer , reviewedDate= :reviewDate, reviewStatus= :status"
            						+" where claimId= :id"
            			);
        	query.setParameter("reviewer", reviewedBy);
        	query.setParameter("reviewDate", new Date());
        	query.setParameter("status", "Approve");
        	query.setParameter("id", claimId);

        	query.executeUpdate();

        	tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
