package com.precorconnect.claimspiffservice.okta;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;

interface GetPartnerRepNameWithIdFeature {

    String execute(
            @NonNull UserId emailAddress
    );

}
