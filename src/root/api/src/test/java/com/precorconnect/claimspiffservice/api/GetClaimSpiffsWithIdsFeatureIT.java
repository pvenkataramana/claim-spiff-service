package com.precorconnect.claimspiffservice.api;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/GetClaimSpiffsWithIds.feature"},
        glue = {"com.precorconnect.claimspiffservice.api.getclaimspiffswithidsfeature"}
        )
public class GetClaimSpiffsWithIdsFeatureIT {

}
