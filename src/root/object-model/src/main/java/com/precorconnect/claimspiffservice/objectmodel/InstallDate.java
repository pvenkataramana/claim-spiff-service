package com.precorconnect.claimspiffservice.objectmodel;

import java.util.Date;

public interface InstallDate {

	Date getValue();
	
}
