package com.precorconnect.claimspiffservice.salesforce;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;

interface GetAccountNameWithIdFeature {

    String execute(
            @NonNull AccountId id
    );

}
