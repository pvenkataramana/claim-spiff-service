package com.precorconnect.claimspiffservice.database;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ClaimSpiffRequestFactoryImplTest {

	/*
    fields
	*/
    private  Dummy dummy = new Dummy();
    
    private final ClaimSpiffRequestFactoryImpl claimSpiffRequestFactoryImpl=new ClaimSpiffRequestFactoryImpl();
    
    /*
    test methods
	*/
    @Test(expected = IllegalArgumentException.class)
    public void testConstruct_whenClaimSpiffDtoNull_shouldThrowIllegalArgumentException(
    		)throws Exception{
    	claimSpiffRequestFactoryImpl
    	.construct(
    			null
    			);
    }
    
    @Test
    public void testConstruct_whenClaimSpiffDtoFromDummy_ShouldReturnClaimSpiffDtoObj(){
    	ClaimSpiff claimSpiff=claimSpiffRequestFactoryImpl
				    			.construct(
				    					dummy.getClaimSpiffDto()
				    				);
    	
    	
    	
    	
    	assertEquals(
    			dummy.getClaimSpiff(),
    			claimSpiff
    			);
    }
}
