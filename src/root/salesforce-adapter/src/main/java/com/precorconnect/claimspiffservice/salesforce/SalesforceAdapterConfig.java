package com.precorconnect.claimspiffservice.salesforce;

import java.net.URL;

import com.precorconnect.OAuth2ClientCredentials;
import com.precorconnect.Password;
import com.precorconnect.Username;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfig;

public interface SalesforceAdapterConfig {

    URL getBaseOAuth2Url();

    URL getInstanceBaseUrl();

    Username getUsername();

    Password getPassword();

    OAuth2ClientCredentials getConnectedAppCredentials();

    OAuth2ClientCredentials getPrecorConnectClientCredentials();

    IdentityServiceSdkConfig getIdentityServiceSdkConfig();


}
