package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

@Singleton
public class SpiffLogRequestFactoryImpl implements SpiffLogRequestFactory {

	@Override
	public ClaimSpiff construct(
			@NonNull SpiffLogDto spiffLogDto
			) {

		 guardThat(
                 "spiffLogDto",
                 spiffLogDto
               )
                .isNotNull();


		 ClaimSpiff entityObject = new ClaimSpiff();

		 entityObject
		 	.setClaimId(
		 			spiffLogDto
		 				.getClaimSpiffId()
		 				.getValue()
		 			);

		 entityObject
			.setPartnerSaleRegistrationId(
					spiffLogDto
						.getPartnerSaleRegistrationId()
						.getValue()
							);

		 entityObject
			.setPartnerAccountId(
				spiffLogDto
					.getPartnerAccountId()
					.getValue()
						);

		 entityObject
			.setPartnerRepUserId(
				spiffLogDto
					.getPartnerRepUserId()
					.getValue()
						);

		 entityObject
		 	.setSellDate(
		 			spiffLogDto
		 				.getSellDate()
		 				.getValue()
		 			);

		 entityObject
			.setInstallDate(
				spiffLogDto
					.getInstallDate()
					.getValue()
				);

		 entityObject
			.setSpiffAmount(
				spiffLogDto
					.getSpiffAmount()
					.getValue()
						);

		 entityObject
			.setSpiffClaimedDate(
				spiffLogDto
						.getSpiffClaimedDate()
						.getValue()
								);


		 entityObject
			.setFacilityName(
					spiffLogDto
						.getFacilityName()
						.getValue()
							);

		 entityObject
			.setInvoiceNumber(
					spiffLogDto
						.getInvoiceNumber()
						.getValue()
							);

		 entityObject
		 	.setReviewStatus(
		 			spiffLogDto
		 				.getReviewStatus()
		 				.getValue()
		 			);

		 entityObject
		 	.setReviewedBy(
		 			spiffLogDto
		 				.getReviewedBy()
		 				.getValue()
		 			);

		 entityObject
		 	.setReviewedDate(
		 			spiffLogDto
		 				.getReviewedDate()
		 				.getValue()
		 			);

		 return
        		entityObject;

	}

}
