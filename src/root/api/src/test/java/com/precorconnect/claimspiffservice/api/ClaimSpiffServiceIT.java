package com.precorconnect.claimspiffservice.api;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

public class ClaimSpiffServiceIT {

	 /*
    fields
     */
	private Dummy dummy = new Dummy();

	private Config config = new ConfigFactory()
										.construct();

	private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

	private ClaimSpiffServiceImpl claimSpiffService=
					new ClaimSpiffServiceImpl(
											config.
											getClaimSpiffServiceConfig()
										);

	/*
    test methods
	*/

	@Test
	public void testaddCliamSpiffs_whenClaimSpiffDtoList_shownClaimSpiffIdList() throws AuthenticationException{

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
																factory
																	.constructValidPartnerRepOAuth2AccessToken()
																	.getValue()
																);
		Collection<ClaimSpiffId> listClaimIds=
							claimSpiffService.addCliamSpiffs(
											dummy.getClaimSpiffDtoList(),
											accessToken
										);

		assertEquals(
				dummy.getListClaimSpiffId().size(),
				listClaimIds.size()
				);

	}

	@Test
	public void testgetClaimSpiffsWithIds_whenListClaimSpiffsIds_shownClaimSpiffList() throws AuthenticationException{

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
																factory
																	.constructValidPartnerRepOAuth2AccessToken()
																	.getValue()
																);

		Collection<ClaimSpiffView> claimSpiffViewsList=
								claimSpiffService.getClaimSpiffsWithIds(
										dummy.getListClaimSpiffId(),
										accessToken
										);

		assertEquals(
				dummy.getListClaimSpiffId().size(),
				claimSpiffViewsList.size()
				);

	}

	/*
    test methods
	*/
	@Test
	public void testCreateSpiffEntitlements_whenSpiffEntitlementDtoList_shownSpiffEntitlementIdList() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
												);

		Collection<SpiffEntitlementId> spiffEntitlementIdList = claimSpiffService
																		.createSpiffEntitlements(
																				dummy.getSpiffEntitlementDtoList(),
																				accessToken
																				);
		assertEquals(
				dummy.getSpiffEntitlementIdList().size(),
				spiffEntitlementIdList.size()
				);
	}

	@Test
	public void testlistEntitlementsWithPartnerId_whenAccountId_shownSpiffEntitlementList() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
												);

		Collection<SpiffEntitlementView> spiffEntitlementViewList = claimSpiffService
																		.listEntitlementsWithPartnerId(
																				dummy.getPartnerAccountId(),
																				accessToken
																				);

		assertThat(spiffEntitlementViewList.size())
					.isGreaterThan(0);
	}

	@Test
	public void testupdateInvoiceUrl_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
												);

		claimSpiffService
						.updateInvoiceUrl(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewInvoiceUrl(),
								accessToken
								);

	}

	@Test
	public void testupdatePartnerRep_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
												);

		claimSpiffService
						.updatePartnerRep(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewPartnerRepUserId(),
								accessToken
								);

	}


}
