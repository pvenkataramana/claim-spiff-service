package com.precorconnect.claimspiffservice.objectmodel;

public interface FacilityId {

	Long getValue();
}
