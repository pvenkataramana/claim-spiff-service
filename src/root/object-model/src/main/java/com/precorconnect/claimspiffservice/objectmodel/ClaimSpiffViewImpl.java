package com.precorconnect.claimspiffservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public class ClaimSpiffViewImpl implements ClaimSpiffView {

	/*
    fields
     */
	private final ClaimSpiffId claimId;

    private final PartnerSaleRegistrationId partnerSaleRegistrationId;

    private final AccountId partnerAccountId;

    private final UserId partnerRepUserId;

    private final InstallDate installDate;

    private final SpiffAmount spiffAmount;

    private final SpiffClaimedDate spiffClaimedDate;

    private final FacilityName facilityName;

    private final InvoiceNumber invoiceNumber;

    private final SellDate sellDate;

    public ClaimSpiffViewImpl(
			@NonNull ClaimSpiffId claimId,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull AccountId partnerAccountId,
			@NonNull UserId partnerRepUserId,
			@NonNull InstallDate installDate,
			@NonNull SpiffAmount spiffAmount,
			@NonNull SpiffClaimedDate spiffClaimedDate,
			@NonNull FacilityName facilityName,
			@NonNull InvoiceNumber invoiceNumber,
			@NonNull SellDate sellDate
			) {

		this.claimId =
                guardThat(
                        "claimId",
                         claimId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                         partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId =
                guardThat(
                        "partnerRepUserId",
                         partnerRepUserId
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;


		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                         spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffClaimedDate =
                guardThat(
                        "spiffClaimedDate",
                         spiffClaimedDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                         facilityName
                )
                        .isNotNull()
                        .thenGetValue();


		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                         invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

	}

	public ClaimSpiffId getClaimId() {
		return claimId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountId getPartnerAccountId() {
		return partnerAccountId;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffClaimedDate getSpiffClaimedDate() {
		return spiffClaimedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((claimId == null) ? 0 : claimId.hashCode());
		result = prime * result
				+ ((facilityName == null) ? 0 : facilityName.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime
				* result
				+ ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime
				* result
				+ ((partnerRepUserId == null) ? 0 : partnerRepUserId.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result
				+ ((spiffAmount == null) ? 0 : spiffAmount.hashCode());
		result = prime
				* result
				+ ((spiffClaimedDate == null) ? 0 : spiffClaimedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClaimSpiffViewImpl other = (ClaimSpiffViewImpl) obj;
		if (claimId == null) {
			if (other.claimId != null) {
				return false;
			}
		} else if (!claimId.equals(other.claimId)) {
			return false;
		}
		if (facilityName == null) {
			if (other.facilityName != null) {
				return false;
			}
		} else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
		if (installDate == null) {
			if (other.installDate != null) {
				return false;
			}
		} else if (!installDate.equals(other.installDate)) {
			return false;
		}
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null) {
				return false;
			}
		} else if (!invoiceNumber.equals(other.invoiceNumber)) {
			return false;
		}
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null) {
				return false;
			}
		} else if (!partnerAccountId.equals(other.partnerAccountId)) {
			return false;
		}
		if (partnerRepUserId == null) {
			if (other.partnerRepUserId != null) {
				return false;
			}
		} else if (!partnerRepUserId.equals(other.partnerRepUserId)) {
			return false;
		}
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null) {
				return false;
			}
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId)) {
			return false;
		}
		if (sellDate == null) {
			if (other.sellDate != null) {
				return false;
			}
		} else if (!sellDate.equals(other.sellDate)) {
			return false;
		}
		if (spiffAmount == null) {
			if (other.spiffAmount != null) {
				return false;
			}
		} else if (!spiffAmount.equals(other.spiffAmount)) {
			return false;
		}
		if (spiffClaimedDate == null) {
			if (other.spiffClaimedDate != null) {
				return false;
			}
		} else if (!spiffClaimedDate.equals(other.spiffClaimedDate)) {
			return false;
		}
		return true;
	}

}