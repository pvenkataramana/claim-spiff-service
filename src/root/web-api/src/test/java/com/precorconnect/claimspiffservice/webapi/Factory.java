package com.precorconnect.claimspiffservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.SapVendorNumberImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.identityservice.PartnerRepJwt;
import com.precorconnect.identityservice.PartnerRepJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;

public class Factory {

    /*
    fields
     */
    private final Dummy dummy;

    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    /*
    constructors
     */
    public Factory(
            @NonNull Dummy dummy,
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk
    ) {

        this.dummy =
                guardThat(
                        "dummy",
                        dummy
                )
                        .isNotNull()
                        .thenGetValue();

        this.identityServiceIntegrationTestSdk =
                guardThat(
                        "identityServiceIntegrationTestSdk",
                        identityServiceIntegrationTestSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    factory methods
    */
    public OAuth2AccessToken constructValidPartnerRepOAuth2AccessToken() {

    	PartnerRepJwt partnerRepJwt =
                new PartnerRepJwtImpl(
                        Instant.now().plusSeconds(86400),
                        dummy.getUri(),
                        dummy.getUri(),
                        new FirstNameImpl("venkataramana"),
                        new LastNameImpl("P"),
                        new UserIdImpl("00u5h8rhbtzPEtCH20h7"),
                        new AccountIdImpl("001K000001H2Km2IAF"),
                        new SapVendorNumberImpl("0000000000")
                );


        return
                identityServiceIntegrationTestSdk
                	.getPartnerRepOAuth2AccessToken(partnerRepJwt);
    }

}
