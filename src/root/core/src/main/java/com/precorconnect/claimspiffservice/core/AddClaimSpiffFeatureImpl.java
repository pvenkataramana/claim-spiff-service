package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

@Singleton
public class AddClaimSpiffFeatureImpl implements
		AddClaimSpiffFeature{

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public AddClaimSpiffFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<ClaimSpiffId> addClaimSpiffs(
			@NonNull Collection<ClaimSpiffDto> listClaimSpiffs
		    ) throws AuthenticationException{

		return
				databaseAdapter
					.addClaimSpiffs(
							listClaimSpiffs
						);
	}


}
