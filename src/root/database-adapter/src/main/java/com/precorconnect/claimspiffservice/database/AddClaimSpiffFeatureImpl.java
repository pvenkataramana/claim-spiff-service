package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;

@Singleton
public class AddClaimSpiffFeatureImpl implements
	AddClaimSpiffFeature {

    private final SessionFactory sessionFactory;
    private final ClaimSpiffRequestFactory claimSpiffRequestFactory;

    @Inject
    public AddClaimSpiffFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final ClaimSpiffRequestFactory claimSpiffRequestFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();


    	this.claimSpiffRequestFactory =
                guardThat(
                        "claimSpiffRequestFactory",
                         claimSpiffRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
	public Collection<ClaimSpiffId> addClaimSpiffs(
			Collection<ClaimSpiffDto> listClaimSpiffs
			) {

		List<ClaimSpiffId> claimSpiffIds = new ArrayList<ClaimSpiffId>();

		List<SpiffEntitlementId> spiffEntitlementIds = new ArrayList<SpiffEntitlementId>();

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		try {
              	for(ClaimSpiffDto claimSpiffObj : listClaimSpiffs){

            		Long claimId =  (Long) session.save(
            								claimSpiffRequestFactory.
            											construct(
            													claimSpiffObj
            											)
            										);

            		claimSpiffIds
            			.add(
            					new ClaimSpiffIdImpl(claimId)
            				);

            		spiffEntitlementIds.add(claimSpiffObj.getSpiffEntitlementId());

                }

        		List<Long> spiffEntitlementIdsList = spiffEntitlementIds
																.stream()
																.map(id-> id.getValue())
																.collect(Collectors.toList());

        		Query query = session.createQuery("Delete from SpiffEntitlement se  where se.spiffEntitlementId in (:idList)");

        		query.setParameterList("idList", spiffEntitlementIdsList);

        		query.executeUpdate();

              	tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("Exception while committing transaction:", e);

        } finally {

        	if (session != null) {
                session.close();
            }
        }
        return claimSpiffIds;
	}


}
