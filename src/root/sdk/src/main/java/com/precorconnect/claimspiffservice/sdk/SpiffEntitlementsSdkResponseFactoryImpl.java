package com.precorconnect.claimspiffservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InstallDate;
import com.precorconnect.claimspiffservice.objectmodel.InstallDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementViewImpl;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;

@Singleton
public class SpiffEntitlementsSdkResponseFactoryImpl implements
		SpiffEntitlementsSdkResponseFactory {

	@Override
	public SpiffEntitlementView construct(
			@NonNull SpiffEntitlementWebView spiffEntitlementWebView) {

		guardThat("spiffEntitlementWebView", spiffEntitlementWebView)
				.isNotNull();

		SpiffEntitlementId spiffEntitlementId = new SpiffEntitlementIdImpl(
				spiffEntitlementWebView.getSpiffEntitlementId());

		AccountId accountId = new AccountIdImpl(
				spiffEntitlementWebView.getAccountId());

		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(
				spiffEntitlementWebView.getPartnerSaleRegistrationId());

		FacilityName facilityName = new FacilityNameImpl(
				spiffEntitlementWebView.getFacilityName());

		InvoiceNumber invoiceNumber = new InvoiceNumberImpl(
				spiffEntitlementWebView.getInvoiceNumber());

		InvoiceUrl invoiceUrl = new InvoiceUrlImpl(
				spiffEntitlementWebView.getInvoiceUrl());

		UserId partnerRepUserId = null;

		if (spiffEntitlementWebView.getPartnerRepUserId() != null) {
			partnerRepUserId = new UserIdImpl(
					spiffEntitlementWebView.getPartnerRepUserId());
		}

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		Date date_install;
		try {
			date_install = dateFormat.parse(spiffEntitlementWebView.getInstallDate());
		} catch (ParseException e) {
			throw new RuntimeException("install date parse exception: ", e);
		}

		InstallDate installDate = new InstallDateImpl(date_install);

		SpiffAmount spiffAmount = new SpiffAmountImpl(
				spiffEntitlementWebView.getSpiffAmount());

		Date date_sell;
		try {
			date_sell = dateFormat.parse(spiffEntitlementWebView.getSellDate());
		} catch (ParseException e) {
			throw new RuntimeException("sell date parse exception: ", e);
		}

		SellDate sellDate = new SellDateImpl(date_sell);

		return
				new SpiffEntitlementViewImpl(
										spiffEntitlementId,
										accountId,
										partnerSaleRegistrationId,
										facilityName,
										invoiceNumber,
										invoiceUrl,
										partnerRepUserId,
										installDate,
										spiffAmount,
										sellDate
										);
	}

}
