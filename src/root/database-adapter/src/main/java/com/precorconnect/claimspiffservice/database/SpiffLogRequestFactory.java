package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogDto;

public interface SpiffLogRequestFactory {

	ClaimSpiff construct(
    		  @NonNull SpiffLogDto claimSpiffDto
    );

}
