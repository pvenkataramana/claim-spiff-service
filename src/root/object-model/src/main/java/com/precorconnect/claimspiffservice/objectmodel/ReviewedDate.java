package com.precorconnect.claimspiffservice.objectmodel;

import java.util.Date;

public interface ReviewedDate {

	Date getValue();

}
