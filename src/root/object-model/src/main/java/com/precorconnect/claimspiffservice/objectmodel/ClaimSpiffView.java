package com.precorconnect.claimspiffservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.UserId;

public interface ClaimSpiffView {

	/*
    fields
     */
	  ClaimSpiffId getClaimId();

      PartnerSaleRegistrationId getPartnerSaleRegistrationId();

      AccountId getPartnerAccountId();

      UserId getPartnerRepUserId();

      InstallDate getInstallDate();

      SpiffAmount getSpiffAmount();

      SpiffClaimedDate getSpiffClaimedDate();

      FacilityName getFacilityName();

      InvoiceNumber getInvoiceNumber();

      SellDate getSellDate();

}