package com.precorconnect.claimspiffservice.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;

public class ClaimSpiffResponseFactoryImplTest {

	/*
    fields
	*/
    private  final Dummy dummy = new Dummy();
    
    private final ClaimSpiffResponseFactoryImpl claimSpiffResponseFactoryImpl=
    		new ClaimSpiffResponseFactoryImpl();
    
    /*
    test methods
	*/
    
    @Test(expected= IllegalArgumentException.class)
    public void testConstruct_whenClaimSpiffNull_ShouldThrowIllegalArgumentException(
    		)throws Exception{
    	claimSpiffResponseFactoryImpl
    						.construct(
    								null
    						);
    }
    
    @Test
    public void testConstruct_whenClaimSpiffFromDummy_shouldReturnClaimSpiffViewObj(
    		){
   ClaimSpiffView claimSpiffView= 	claimSpiffResponseFactoryImpl
    							.construct(
    									dummy
    									.getClaimSpiff()
    									);
       	
    	
    	assertEquals(
    			dummy.getClaimSpiffView(),
    			claimSpiffView
    			);
    }
}
