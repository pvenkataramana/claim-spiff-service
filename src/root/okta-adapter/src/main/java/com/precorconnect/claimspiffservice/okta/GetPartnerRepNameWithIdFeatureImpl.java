package com.precorconnect.claimspiffservice.okta;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.okta.sdk.clients.UserApiClient;
import com.okta.sdk.models.users.User;
import com.precorconnect.UserId;

@Singleton
class GetPartnerRepNameWithIdFeatureImpl
        implements GetPartnerRepNameWithIdFeature {

    /*
    fields
     */
    private final UserApiClient userApiClient;

    /*
    constructors
     */
    @Inject
    public GetPartnerRepNameWithIdFeatureImpl(
            @NonNull UserApiClient userApiClient
    ) {

    	this.userApiClient =
                guardThat(
                        "userApiClient",
                         userApiClient
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public String execute(
            @NonNull UserId partnerRepId
    ) {

        User user;

        try {

            user = userApiClient
                    .getUser(
                            partnerRepId
                                    .getValue()
                    );

        } catch (IOException e) {

            throw new RuntimeException(e);

        }

        String partnerRepFullName = user.getProfile().getFirstName()+
        							" "+
                					user.getProfile().getLastName();
        return
        		partnerRepFullName;
    }
}
