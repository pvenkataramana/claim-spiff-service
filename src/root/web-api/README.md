## Description
Precor Connect calim spiff service API for ReST.

## Features

##### Add List of Claim Spiffs
* [documentation](features/AddClaimSpiffs.feature)

##### List Claim Spiffs With PartnerId
* [documentation](features/GetClaimSpiffsWithIds.feature)

##### Create Spiff Entitlements
* [documentation](features/CreateSpiffEntitlements.feature)

##### List Spiff Entitlements With Id
* [documentation](features/ListEntitlementsWithPartnerId.feature)

##### Update Invoice Url
* [documentation](features/UpdateInvoiceUrl.feature)

##### Update Partner Rep
* [documentation](features/UpdatePartnerRep.feature)



## API Explorer

##### Environments:
-  [dev](https://cliam-spiff-service-dev.precorconnect.com/)
-  [qa](https://claim-spiff-service-qa.precorconnect.com/)
-  [prod](https://claim-spiff-service-prod.precorconnect.com/)