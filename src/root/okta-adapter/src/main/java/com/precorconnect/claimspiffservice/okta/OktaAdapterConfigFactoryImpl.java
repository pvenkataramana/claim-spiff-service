package com.precorconnect.claimspiffservice.okta;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.OAuth2ClientCredentialsImpl;
import com.precorconnect.OAuth2ClientId;
import com.precorconnect.OAuth2ClientIdImpl;
import com.precorconnect.OAuth2ClientSecret;
import com.precorconnect.OAuth2ClientSecretImpl;

/**
 * Constructs {@link OktaAdapterConfig}
 * from system properties
 */
public class OktaAdapterConfigFactoryImpl
        implements OktaAdapterConfigFactory {

    @Override
    public OktaAdapterConfig construct() {

        return
                new OktaAdapterConfigImpl(
                        constructOktaBaseUrl(),
                        constructOktaApiToken(),
                        new OAuth2ClientCredentialsImpl(
                                constructPrecorConnectClientId(),
                                constructPrecorConnectClientSecret()
                        )
                );

    }


    private URL constructOktaBaseUrl() {

        String baseUrl = System.getenv("OKTA_BASE_URL");

        try {

            return new URL(baseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }

    private OktaApiToken constructOktaApiToken() {

        String apiToken = System.getenv("OKTA_API_TOKEN");

        return new OktaApiTokenImpl(apiToken);

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }

    private OAuth2ClientId constructPrecorConnectClientId() {

        String precorConnectClientId = System.getenv("PRECOR_CONNECT_CLIENT_ID");

        return
                new OAuth2ClientIdImpl(
                        precorConnectClientId
                );

    }

    private OAuth2ClientSecret constructPrecorConnectClientSecret() {

        String precorConnectClientSecret = System.getenv("PRECOR_CONNECT_CLIENT_SECRET");

        return
                new OAuth2ClientSecretImpl(
                        precorConnectClientSecret
                );
    }


}