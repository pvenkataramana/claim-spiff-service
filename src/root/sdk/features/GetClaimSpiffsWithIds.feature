Feature: Get Claim SPIFFs with Ids
  Get Submitted Claim SPIFF

  Background:
    Given a claimSpiffId list consists of:
      | attribute     				| validation | type   |
      | claimSpiffId     			| required   | string |
      
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid claimSpiffId list
    When I execute getClaimSpiffsWithIds
    Then the claimedspiffs with the matched claimSpiffId are returned