package com.precorconnect.claimspiffservice.sdk;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;


public interface ClaimSpiffSdkResponseFactory {

	ClaimSpiffView construct(
			ClaimSpiffWebView claimSpiffView
			);
}
