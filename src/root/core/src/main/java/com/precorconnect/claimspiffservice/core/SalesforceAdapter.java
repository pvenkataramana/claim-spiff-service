package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;

public interface SalesforceAdapter {

    String getAccountNameWithId(
            @NonNull AccountId accountId
    );


}
