package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;


public interface ClaimSpiffRequestFactory {

	ClaimSpiff construct(
    		  @NonNull ClaimSpiffDto claimSpiffDto
    );

}
