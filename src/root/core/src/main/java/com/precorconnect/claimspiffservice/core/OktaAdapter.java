package com.precorconnect.claimspiffservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;

public interface OktaAdapter {

    String getPartnerRepNameWithId(
            @NonNull UserId partnerRepId
    );

}
