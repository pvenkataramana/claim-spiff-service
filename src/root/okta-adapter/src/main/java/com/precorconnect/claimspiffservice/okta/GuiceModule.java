package com.precorconnect.claimspiffservice.okta;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.okta.sdk.clients.UserApiClient;
import com.okta.sdk.framework.ApiClientConfiguration;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final OktaAdapterConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull final OktaAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

    }

    private void bindFeatures() {

        bind(GetPartnerRepNameWithIdFeature.class)
        		.to(GetPartnerRepNameWithIdFeatureImpl.class);

    }

    @Provides
    OktaAdapterConfig userStoreAdapterConfig() {

        return config;

    }


    @Provides
    @Singleton
    UserApiClient userApiClient() {

        return
                new UserApiClient(
                        new ApiClientConfiguration(
                                config.getOktaBaseUrl().toString(),
                                config.getOktaApiToken().getValue()
                        )
                );

    }

}
