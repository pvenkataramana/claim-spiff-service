package com.precorconnect.claimspiffservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;

public interface SpiffEntitlementsWebRequestFactory {

	SpiffEntitlementDto construct(
    		  @NonNull SpiffEntitlementWebDto spiffEntitlementWebDto
    );

}
