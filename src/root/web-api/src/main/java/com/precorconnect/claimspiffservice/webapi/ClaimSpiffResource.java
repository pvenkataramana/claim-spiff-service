package com.precorconnect.claimspiffservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebView;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.SpiffEntitlementWebView;

@RestController
@RequestMapping("/claim-spiffs")
@Api(value = "/claim-spiffs", description = "Operations on calim spiffs")
public class ClaimSpiffResource {

    /*
    fields
     */
    private final ClaimSpiffService claimSpiffService;

    private final ClaimSpiffWebRequestFactory claimSpiffViewRequestFactory;

    private final ClaimSpiffViewResponseFactory claimSpiffViewResponseFactory;

    private final SpiffEntitlementsWebResponseFactory spiffEntitlementsWebResponseFactory;

    private final SpiffEntitlementsWebRequestFactory spiffEntitlementsWebRequestFactory;

    private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

    @Inject
    public ClaimSpiffResource(
            @NonNull final ClaimSpiffService claimSpiffService,
            @NonNull final ClaimSpiffWebRequestFactory claimSpiffViewRequestFactory,
            @NonNull final ClaimSpiffViewResponseFactory claimSpiffViewResponseFactory,
            @NonNull final SpiffEntitlementsWebResponseFactory spiffEntitlementsWebResponseFactory,
            @NonNull final SpiffEntitlementsWebRequestFactory spiffEntitlementsWebRequestFactory,
            @NonNull final OAuth2AccessTokenFactory oAuth2AccessTokenFactory
    ) {

    	this.claimSpiffService =
                guardThat(
                        "claimSpiffService",
                         claimSpiffService
                )
                        .isNotNull()
                        .thenGetValue();

    	this.claimSpiffViewRequestFactory =
                guardThat(
                        "claimSpiffViewRequestFactory",
                         claimSpiffViewRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.claimSpiffViewResponseFactory =
                guardThat(
                        "claimSpiffViewResponseFactory",
                         claimSpiffViewResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementsWebResponseFactory =
                guardThat(
                        "spiffEntitlementsWebResponseFactory",
                        spiffEntitlementsWebResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementsWebRequestFactory =
                guardThat(
                        "spiffEntitlementsWebRequestFactory",
                        spiffEntitlementsWebRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oAuth2AccessTokenFactory =
                guardThat(
                        "oAuth2AccessTokenFactory",
                         oAuth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @RequestMapping(
    		method = RequestMethod.POST
    		)
    @ApiOperation(
    		value = "Insert list of calim spiffs"
    		)
    public Collection<Long> addClaimSpiffs(
    		@RequestBody List<ClaimSpiffWebDto> listClaimSpiffView,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException{

    	OAuth2AccessToken accessToken = oAuth2AccessTokenFactory
                        					.construct(authorizationHeader);


    	List<ClaimSpiffDto> claimSpiffDtos = new ArrayList<ClaimSpiffDto>();

    	for(ClaimSpiffWebDto viewObject: listClaimSpiffView){
    		claimSpiffDtos.add(claimSpiffViewRequestFactory.construct(viewObject));
    	}

   		Collection<Long> claimIds =
   				claimSpiffService.
   				addCliamSpiffs(
   						claimSpiffDtos,
   						accessToken
   						).stream()
   				         .map(claimSpiffId -> claimSpiffId.getValue())
   				         .collect(Collectors.toList());

   		return claimIds;

    }

    @RequestMapping(
            value = "claimids/{claimIds}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON
            )
    @ApiOperation(
            value = "gets the claim spiffs with the provided ids"
            )
    public Collection<ClaimSpiffWebView> getClaimSpiffsWithIds(
    		@PathVariable("claimIds") ArrayList<Long> claimIds,
            @RequestHeader("Authorization") String authorizationHeader
    ) throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken = oAuth2AccessTokenFactory
											.construct(authorizationHeader);

        List<ClaimSpiffId> claimSpiffIds = claimIds.stream()
        										   .map(id -> new ClaimSpiffIdImpl(id))
        										   .collect(Collectors.toList());
        return
        		claimSpiffService
                        .getClaimSpiffsWithIds(claimSpiffIds, accessToken)
                        .stream()
                        .map(claimSpiffViewResponseFactory::construct)
                        .collect(Collectors.toList());

    }

    @RequestMapping(
    		value="/entitlements",
    		method = RequestMethod.POST
    		)
    @ApiOperation(
    		value = "create/add list spiff entitlements"
    		)
    public List<Long> createSpiffEntitlements(
    		@RequestBody List<SpiffEntitlementWebDto> webSpiffentitlementDtos,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

    	List<SpiffEntitlementDto> coreSpiffentitlementDtos = new ArrayList<SpiffEntitlementDto>();

    	for(SpiffEntitlementWebDto viewObject: webSpiffentitlementDtos){

    		coreSpiffentitlementDtos
    				.add(spiffEntitlementsWebRequestFactory
    						.construct(
    								viewObject
    								)
    					);
    	}



        List<Long> entitlementIds = claimSpiffService
        						.createSpiffEntitlements(
        								coreSpiffentitlementDtos,
										accessToken
        								).stream()
										 .map(id -> id.getValue())
										 .collect(Collectors.toList());


        return
        		entitlementIds;

    }

    @RequestMapping(
    		value="id/{accountId}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON
    		)
    @ApiOperation(
    		value = "Lists all spiff entitlements with specified partner Id"
    		)
    public List<SpiffEntitlementWebView> listEntitlementsWithPartnerId(
    		@PathVariable("accountId") String accountId,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);


    	return
    			claimSpiffService
    			 	.listEntitlementsWithPartnerId(
    			 						new AccountIdImpl(accountId),
    			 						accessToken
    			 						)
    			 						.stream()
    			 						.map(spiffEntitlementsWebResponseFactory::construct)
    			 						.collect(Collectors.toList()
    			 					);

    }

    @RequestMapping(
    		value="/entitlements/{partnerSaleRegistrationId}/invoiceurl",
    		method = RequestMethod.POST
    		)
    @ApiOperation(
    		value = "update the invoice url when ever there is new invoice uploaded"
    		)
    public void updateInvoiceUrl(
    		@PathVariable("partnerSaleRegistrationId") Long partnerSaleRegistrationId,
    		@RequestBody String invoiceUrl,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

    	claimSpiffService
                        .updateInvoiceUrl(
                        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
                        		new InvoiceUrlImpl(invoiceUrl),
                        		accessToken
                        		);

    }

    @RequestMapping(
    		value="/entitlements/{partnerSaleRegistrationId}/partnerrepuserid/{partnerRepUserId}",
    		method = RequestMethod.POST
    		)
    @ApiOperation(
    		value = "update the partner rep when ever there is change in the partner rep"
    		)
    public String updatePartnerRep(
    		@PathVariable("partnerSaleRegistrationId") Long partnerSaleRegistrationId,
    		@PathVariable("partnerRepUserId") String partnerRepUserId,
    		@RequestHeader("Authorization") String authorizationHeader
    )throws AuthenticationException, AuthorizationException {

    	OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory
                        .construct(authorizationHeader);

    	claimSpiffService
                        .updatePartnerRep(
                        		new PartnerSaleRegistrationIdImpl(partnerSaleRegistrationId),
                        		new UserIdImpl(partnerRepUserId.trim()),
                        		accessToken
                        		);
    	return "Updated successfully";
    }


}
