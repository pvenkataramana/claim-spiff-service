package com.precorconnect.claimspiffservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;
    
    private final String baseUrl;

    /*
    constructors
     */
    public Config(
    	@NonNull HmacKey identityServiceJwtSigningKey,
    	@NonNull String baseUrl
    		){
    	this.identityServiceJwtSigningKey =
                guardThat(
                		"identityServiceJwtSigningKey",
                        identityServiceJwtSigningKey
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.baseUrl  =
                guardThat(
                		"baseUrl",
                		 baseUrl
                )
                        .isNotNull()
                        .thenGetValue();
    }

    /*
    getter methods
    */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
