package com.precorconnect.claimspiffservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

public interface SpiffLogResponseFactory {

	SpiffLogView construct(
			@NonNull ClaimSpiff spiffLog
			);
}
