package com.precorconnect.claimspiffservice.identity;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;

public class IdentityServiceAdapterConfigImpl
        implements IdentityServiceAdapterConfig {

    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public IdentityServiceAdapterConfigImpl(

            @NonNull final URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                        precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        IdentityServiceAdapterConfigImpl that = (IdentityServiceAdapterConfigImpl) o;

        return precorConnectApiBaseUrl.equals(that.precorConnectApiBaseUrl);

    }

    @Override
    public int hashCode() {
        return precorConnectApiBaseUrl.hashCode();
    }
}
