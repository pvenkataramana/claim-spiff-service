package com.precorconnect.claimspiffservice.api;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfigFactoryImpl;
import com.precorconnect.claimspiffservice.okta.OktaAdapterConfigFactoryImpl;
import com.precorconnect.claimspiffservice.salesforce.SalesforceAdapterConfigFactoryImpl;

public class ClaimSpiffServiceConfigFactoryImpl implements
		ClaimSpiffServiceConfigFactory {

	@Override
	public ClaimSpiffServiceConfig construct() {

		return
				new ClaimSpiffServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
												  .construct(),
						new IdentityServiceAdapterConfigFactoryImpl()
													.construct(),
						new SalesforceAdapterConfigFactoryImpl()
													.construct(),
						new OktaAdapterConfigFactoryImpl()
													.construct()
					);


	}

}
