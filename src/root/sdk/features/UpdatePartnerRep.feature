Feature: Update PartnerRep
  Update PartnerRep
  
  Background:
    Given a partnerSaleRegistrationId and userId consists of:
      | attribute     				| validation | type   |
      | partnerSaleRegistrationId   | required   | number |
      | userId                      | required   | string |

  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid partnerSaleRegistrationId and userId
    When I execute updatePartnerRep
    Then the userId of the matched partnerSaleRegistrationId are modified