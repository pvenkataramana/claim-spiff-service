package com.precorconnect.claimspiffservice.api;

import com.precorconnect.identityservice.HmacKey;
import com.precorconnect.identityservice.HmacKeyImpl;




public final class ConfigFactory {

	public Config construct() {

		String hamckey = System
	             .getenv("TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY");

		HmacKey hamcKey =
				new HmacKeyImpl(
							hamckey
							);

		String baseUrl = System
	             			.getenv("TEST_DEV_PRECONNECT_BASE_URL");

		return
				new Config(
						constructClaimSpiffServiceConfig(),
						hamcKey,
						baseUrl
				);
	}



	private ClaimSpiffServiceConfig constructClaimSpiffServiceConfig() {
		return
				new ClaimSpiffServiceConfigFactoryImpl()
								.construct();
	}

}
