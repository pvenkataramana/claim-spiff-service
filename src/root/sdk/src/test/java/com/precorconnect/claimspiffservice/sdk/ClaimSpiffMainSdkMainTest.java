package com.precorconnect.claimspiffservice.sdk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;


public class ClaimSpiffMainSdkMainTest {

	private final static Dummy dummy =
    		 new Dummy();

    private final static Config config =
            new ConfigFactory()
                    .construct();

    private final static Factory factory =
    		new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );

	public static void main(String[] args) throws Exception{

		OAuth2AccessToken accessToken =
		        factory.constructValidPartnerRepOAuth2AccessToken();

		ClaimSpiffServiceSdk objectUnderTest =
                new ClaimSpiffServiceSdkImpl(
                        new ClaimSpiffServiceSdkConfigFactoryImpl()
                        										.construct()
                );

		addClaimSpiffs(objectUnderTest,accessToken);

		getClaimSpiffsWithIds(objectUnderTest,accessToken);

		createSpiffEntitlement(
											objectUnderTest,
											dummy.getSpiffEntitlementDtosList(),
											accessToken
											);

		listEntitlementsWithPartnerId(
								objectUnderTest,
								dummy.getSpiffEntitlementDto().getAccountId(),
								accessToken
								);

		updateInvoiceUrl(
					objectUnderTest,
					dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
					new InvoiceUrlImpl(dummy.getNewInvoiceUrl()),
					accessToken
					);

		updatePartnerRep(
					objectUnderTest,
					dummy.getSpiffEntitlementDto().getPartnerSaleRegistrationId(),
					new UserIdImpl(dummy.getNewUserId()),
					accessToken
					);

	}

	public static Collection<ClaimSpiffId> addClaimSpiffs(
			ClaimSpiffServiceSdk cilentSdkObject,
			OAuth2AccessToken accessToken
			){

		Collection<ClaimSpiffId> ids = new ArrayList<ClaimSpiffId>();

		try {

			ids.addAll(cilentSdkObject.
					addCliamSpiffs(
							dummy.getClaimSpiffDtoList(),
							accessToken
							));

 	        } catch (Exception e) {

				throw new RuntimeException("Exception occured in addClaimSpiffs: ",e);
			}

		return
				ids;

	}

	public static Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			ClaimSpiffServiceSdk cilentSdkObject,
			OAuth2AccessToken accessToken
			){

		Collection<ClaimSpiffView> claimSpiffs;

		try {
			claimSpiffs = cilentSdkObject.getClaimSpiffsWithIds(
						dummy.getClaimSpiffIds(),
						accessToken
						);
		} catch (AuthenticationException e) {

			throw new RuntimeException("Exception occured in getClaimSpiffsWithIds: ",e);
		}

		return
				claimSpiffs;
	}

	public static Collection<Long> createSpiffEntitlement(
			    ClaimSpiffServiceSdk cilentSdkObject,
				List<SpiffEntitlementDto> spiffEntitlementDtoList,
				OAuth2AccessToken accessToken
			   ){

		   		Collection<Long> ids = new ArrayList<Long>();

		   			try {
		   					ids =
		   							cilentSdkObject.createSpiffEntitlements(
		   												spiffEntitlementDtoList,
		   												accessToken
		   												);
		   			} catch(Exception e){
		   					throw new RuntimeException("Exception occured in createSpiffEntitlement: ",e);
		   				}

		   		return ids;
	}

	public static Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
											ClaimSpiffServiceSdk cilentSdkObject,
											AccountId accountId,
											OAuth2AccessToken accessToken
											){

		Collection<SpiffEntitlementView> spiffEntitlementViewList = new ArrayList<SpiffEntitlementView>();

		try {
			spiffEntitlementViewList =
					cilentSdkObject.listEntitlementsWithPartnerId(
															accountId,
															accessToken
															);
			} catch(Exception e){
				throw new RuntimeException("Exception occured in listEntitlementsWithPartnerId: ",e);
			}

		return spiffEntitlementViewList;
	}

	public static void updateInvoiceUrl(
			ClaimSpiffServiceSdk cilentSdkObject,
			PartnerSaleRegistrationId partnerSaleRegistrationId,
			InvoiceUrl invoiceUrl,
			OAuth2AccessToken accessToken
			){

		try {

			cilentSdkObject.updateInvoiceUrl(
							partnerSaleRegistrationId,
							invoiceUrl,
							accessToken
							);

		} catch(Exception e){
			throw new RuntimeException("Exception occured in updateInvoiceUrl: ",e);
		}

}

	public static void updatePartnerRep(
			ClaimSpiffServiceSdk cilentSdkObject,
			PartnerSaleRegistrationId partnerSaleRegistrationId,
			UserId partnerRepUserId,
			OAuth2AccessToken accessToken
			){

		try {

			cilentSdkObject.updatePartnerRep(
					partnerSaleRegistrationId,
					partnerRepUserId,
					accessToken
					);

		} catch(Exception e){
			throw new RuntimeException("Exception occured in updatePartnerRep: ",e);
		}

	}


}
