package com.precorconnect.claimspiffservice.okta;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.core.OktaAdapter;

public class OktaAdapterImpl
        implements OktaAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public OktaAdapterImpl(
            @NonNull final OktaAdapterConfig config
    ) {

        com.precorconnect.claimspiffservice.okta.GuiceModule guiceModule =
                new com.precorconnect.claimspiffservice.okta.GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);
    }


    @Override
    public String getPartnerRepNameWithId(
            @NonNull final UserId partnerRepId
    ) {

        return
                injector
                        .getInstance(GetPartnerRepNameWithIdFeature.class)
                        .execute(partnerRepId);

    }

}
