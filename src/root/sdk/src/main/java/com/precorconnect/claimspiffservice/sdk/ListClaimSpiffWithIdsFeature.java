package com.precorconnect.claimspiffservice.sdk;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;

interface ListClaimSpiffWithIdsFeature {

    Collection<ClaimSpiffView> getClaimSpiffsWithIds(
    		@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException;
    
}
