package com.precorconnect.claimspiffservice.api;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfig;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfig;
import com.precorconnect.claimspiffservice.okta.OktaAdapterConfig;
import com.precorconnect.claimspiffservice.salesforce.SalesforceAdapterConfig;


public interface ClaimSpiffServiceConfig {

    DatabaseAdapterConfig getDatabaseAdapterConfig();

    IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();

    SalesforceAdapterConfig getSalesforceAdapterConfig();

    OktaAdapterConfig getOktaAdapterConfig();

}
