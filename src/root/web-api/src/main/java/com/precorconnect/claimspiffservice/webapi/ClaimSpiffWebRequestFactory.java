package com.precorconnect.claimspiffservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;


public interface ClaimSpiffWebRequestFactory {
	
	com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto construct(
    		com.precorconnect.claimspiffservice.webapiobjectmodel.
    		  @NonNull ClaimSpiffWebDto claimSpiffView
    );
    
}
