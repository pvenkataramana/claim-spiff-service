package com.precorconnect.claimspiffservice.objectmodel;

public interface ReviewStatus {

	String getValue();

}
