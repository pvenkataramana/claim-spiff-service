package com.precorconnect.claimspiffservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {

    /*
    fields
     */
    private ClaimSpiffServiceConfig claimSpiffServiceConfig;

    private final HmacKey identityServiceJwtSigningKey;

    private final String baseUrl;

    /*
    constructors
     */

  	public Config(
  			@NonNull final ClaimSpiffServiceConfig claimSpiffServiceConfig,
  			@NonNull final HmacKey identityServiceJwtSigningKey,
  			@NonNull final String baseUrl
  			) {

		this.claimSpiffServiceConfig =
				 guardThat("claimSpiffServiceConfig",
						 claimSpiffServiceConfig
	                )
	                        .isNotNull()
	                        .thenGetValue();

		this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

    	this.baseUrl = baseUrl;


	}



    /*
    getter methods
     */

    public ClaimSpiffServiceConfig getClaimSpiffServiceConfig() {
        return claimSpiffServiceConfig;
    }

    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

	public String getBaseUrl() {
		return baseUrl;
	}
}
