﻿Add-Type –AssemblyName System.Windows.Forms

Function Set-EnvironmentFromMavenFailsafePluginConfig(){
    <#
    .SYNOPSIS
    Sets environment variables defined in the maven-failsafe-plugin of the pom.xml located in the same directory.
    Note: this is useful e.g. if trying to debug integration tests or run the applicaton outside of maven.
    #>

    [xml]$rootPomXml = get-content "${PSScriptRoot}/pom.xml"

    $envVars = $($rootPomXml.project.build.plugins.plugin | where {$_.artifactId -like "maven-failsafe-plugin"}).configuration.environmentVariables

    ForEach($envVar in $envVars.GetEnumerator()){

        $envVarName = $envVar.Name
        $envVarValue = $envVar.InnerText.Trim()

        # set in script scope
        Set-Item -Path env:$envVarName -Value $envVarValue -Force

        # set in user scope
        [Environment]::SetEnvironmentVariable($envVarName,$envVarValue,'User')

    }

    [System.Windows.Forms.MessageBox]::Show("You must restart your IDE for Environment to take effect." , "You Must Restart Your IDE!")
}

Set-EnvironmentFromMavenFailsafePluginConfig