package com.precorconnect.claimspiffservice.sdk;


import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.webapiobjectmodel.ClaimSpiffWebDto;

interface ClaimSpiffSdkRequestFactory {

    ClaimSpiffWebDto construct(
            @NonNull ClaimSpiffDto claimSpiffDto
    );

}
