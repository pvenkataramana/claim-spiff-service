package com.precorconnect.claimspiffservice.api.getclaimspiffswithidsfeature;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;
import java.util.List;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.claimspiffservice.api.ClaimSpiffService;
import com.precorconnect.claimspiffservice.api.ClaimSpiffServiceImpl;
import com.precorconnect.claimspiffservice.api.Config;
import com.precorconnect.claimspiffservice.api.ConfigFactory;
import com.precorconnect.claimspiffservice.api.Dummy;
import com.precorconnect.claimspiffservice.api.Factory;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	/*
	 fields
	 */
	private final Config config = new ConfigFactory()
										.construct();

	private final Dummy dummy = new Dummy();

	private final Factory factory =
			new Factory(
						dummy,
						new IdentityServiceIntegrationTestSdkImpl(
									config
										.getIdentityServiceJwtSigningKey()
									)
					);

	private OAuth2AccessToken accessToken;

	private List<ClaimSpiffId> claimSpiffIdsList;

	private Collection<ClaimSpiffView> claimSpiffs;

	@Given("^a claimSpiffId list consists of:$")
	public void aclaimSpiffIdlistconsistsof(
			DataTable arg
			)throws Throwable{
		// no op, inputs are taken from dummy
	}

   @Given("^I provide an accessToken identifying me as a partner rep$")
   public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
   ) throws Throwable {

       accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidPartnerRepOAuth2AccessToken()
													.getValue()
											);

   }

   @Given("^provide a valid claimSpiffId list$")
   public void provideavalidclaimSpiffIdlist(
   ) throws Throwable {

	   claimSpiffIdsList = dummy.getListClaimSpiffId();

   }

   @When("^I execute getClaimSpiffsWithIds$")
   public void IExecutegetClaimSpiffsWithIds(
   ) throws Throwable {

	   ClaimSpiffService objectUnderTest =
								new ClaimSpiffServiceImpl(
											config.getClaimSpiffServiceConfig()
											);

	   claimSpiffs = objectUnderTest
               				.getClaimSpiffsWithIds(
               							claimSpiffIdsList,
               							accessToken
               							);

   }

   @Then("^the claimedspiffs with the matched claimSpiffId are returned$")
   public void theuseridofthematchedpartnersaleregistrationidaremodified(
   ) throws Throwable {

	   assertThat(claimSpiffs.size())
						.isGreaterThan(0);

   }
}
