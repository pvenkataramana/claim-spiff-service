package com.precorconnect.claimspiffservice.okta;

interface OktaApiToken {

    String getValue();

}
