package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountNameImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.UsernameImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffLogView;

@Singleton
public class ListSpiffLogsFeatureImpl implements ListSpiffLogsFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    private final SalesforceAdapter salesforceAdapter;

    private final OktaAdapter oktaAdapter;

    /*
    constructors
     */
    @Inject
    public ListSpiffLogsFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final SalesforceAdapter salesforceAdapter,
            @NonNull final OktaAdapter oktaAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.salesforceAdapter =
                guardThat(
                        "salesforceAdapter",
                        salesforceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oktaAdapter =
                guardThat(
                        "oktaAdapter",
                        oktaAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffLogView> listSpiffLogs()
			throws AuthenticationException {

		Collection<SpiffLogView> spiffLogs =
									databaseAdapter
										.listSpiffLogs();
		Collection<SpiffLogView> modifiedSpiffLogs = new ArrayList<SpiffLogView>();

		for(SpiffLogView spiffLog : spiffLogs){
			String partnerName = salesforceAdapter
										.getAccountNameWithId(spiffLog.getPartnerAccountId());
			String partnerRepName = oktaAdapter
										.getPartnerRepNameWithId(spiffLog.getPartnerRepUserId());

			spiffLog.setAccountName(new AccountNameImpl(partnerName));
			spiffLog.setUserName(new UsernameImpl(partnerRepName));

			modifiedSpiffLogs.add(spiffLog);
		}

		return
				modifiedSpiffLogs;

	}

}
