package com.precorconnect.claimspiffservice.sdk;

public interface ClaimSpiffServiceSdkConfigFactory {

	ClaimSpiffServiceSdkConfig construct();
}
