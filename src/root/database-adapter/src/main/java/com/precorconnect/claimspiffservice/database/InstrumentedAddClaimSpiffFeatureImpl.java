package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

public class InstrumentedAddClaimSpiffFeatureImpl implements AddClaimSpiffFeature{

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedAddClaimSpiffFeatureImpl.class);

	private final AddClaimSpiffFeature underlyingFeature;

	@Inject
	public InstrumentedAddClaimSpiffFeatureImpl(@NonNull @UnderlyingFeature final AddClaimSpiffFeature underlyingFeature) {

		this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public Collection<ClaimSpiffId> addClaimSpiffs(
			Collection<ClaimSpiffDto> listClaimSpiffs) {

		long startTime = System.currentTimeMillis();

		Collection<ClaimSpiffId> result = underlyingFeature
				.addClaimSpiffs(listClaimSpiffs);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database addCliamSpiffs call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;
	}

}
