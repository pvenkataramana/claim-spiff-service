package com.precorconnect.claimspiffservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;


public final class InstrumentedListClaimSpiffWithIdsFeatureImpl implements ListClaimSpiffWithIdsFeature {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedListClaimSpiffWithIdsFeatureImpl.class);

	private final ListClaimSpiffWithIdsFeature underlyingFeature;

	@Inject
	public InstrumentedListClaimSpiffWithIdsFeatureImpl(@NonNull @UnderlyingFeature final ListClaimSpiffWithIdsFeature underlyingFeature) {
    	this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();

	}

	@Override
	public Collection<ClaimSpiffView> getClaimSpiffWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds) {

		long startTime = System.currentTimeMillis();

		Collection<ClaimSpiffView> result = underlyingFeature
				.getClaimSpiffWithIds(listClaimSpiffsIds);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database getClaimSpiffWithIds call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;

	}

}
