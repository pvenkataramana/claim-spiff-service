package com.precorconnect.claimspiffservice.sdk;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;

public class ClaimSpiffServiceSdkImpl
        implements ClaimSpiffServiceSdk {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public ClaimSpiffServiceSdkImpl(
            @NonNull ClaimSpiffServiceSdkConfig config
    ) {
        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );
        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
	public Collection<ClaimSpiffId> addCliamSpiffs(
			@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException{
		return
				injector
						.getInstance(AddClaimSpiffFeature.class)
						.addCliamSpiffs(claimSpiffDto, accessToken);
	}

   	@Override
	public Collection<ClaimSpiffView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> listClaimSpiffsIds,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		 return
				 injector
				 		.getInstance(ListClaimSpiffWithIdsFeature.class)
	                    .getClaimSpiffsWithIds(
	                    		listClaimSpiffsIds,
	                    		accessToken
	                    		);
	}

   	@Override
	public Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        .createSpiffEntitlements(
                        		spiffEntitlements,
                        		accessToken
                        		);

	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        .listEntitlementsWithPartnerId(
                        		accountId,
                        		accessToken
                        		);

	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		injector
        		.getInstance(UpdateInvoiceUrlFeature.class)
        		.updateInvoiceUrl(
        				partnerSaleRegistrationId,
        				invoiceUrl,
        				accessToken
        				);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		injector
				.getInstance(UpdatePartnerRepFeature.class)
				.updatePartnerRep(
						partnerSaleRegistrationId,
						partnerRepUserid,
						accessToken
						);

	}


}
