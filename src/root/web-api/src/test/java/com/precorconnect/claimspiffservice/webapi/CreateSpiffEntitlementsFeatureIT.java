package com.precorconnect.claimspiffservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/CreateSpiffEntitlements.feature"},
        glue = {"com.precorconnect.claimspiffservice.webapi.createspiffentitlementsfeature"}
        )
public class CreateSpiffEntitlementsFeatureIT {

}
