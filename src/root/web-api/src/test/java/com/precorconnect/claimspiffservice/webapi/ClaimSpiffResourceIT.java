package com.precorconnect.claimspiffservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import java.util.StringJoiner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class ClaimSpiffResourceIT {

    /*
    fields
     */

    private final Dummy dummy =
    		 new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();


    private final Factory factory =
            new Factory(
            		dummy,
            		new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    )
            );


    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {

        RestAssured.port = port;
    }

    /*
    test methods
     */
    @Test
    public void addClaimSpiffs_ReturnsListOfIds(
    )  {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

    	 given()
         .header(
                 "Authorization",
                 authToken
         )
 		.header(
 				"Content-Type",
 				"application/json"
 				)
         .body(dummy.getClaimSpiffWebDtoList())
         .post("/claim-spiffs")
         .then()
         .assertThat()
         .statusCode(200);

    }

    @Test
    public void getClaimSpiffsWithIds_ReturnsListOfClaimSpiffs(
    )  {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

    	try{

      	  StringJoiner joiner=new StringJoiner(",");
      	  for(Long claimSpiffId:dummy.getClaimSpiffIds()){
      		joiner.add(""+claimSpiffId);
      	  }


    	  given()
          .header(
                  "Authorization",
                  String.format(
                          "Bearer %s",
                          authToken
                  )
          )
          .get("/claim-spiffs/claimids/"
          		+joiner
          )
          .then()
          .assertThat()
          .statusCode(200);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }

    @Test
    public void createSpiffEntitlements_ReturnsListOfIds() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
        		.header(
        				"Content-Type",
        				"application/json"
        				)
                .body(dummy.getSpiffEntitlementWebDtos())
                .post("/claim-spiffs/entitlements")
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void listEntitlementsWithPartnerId_ReturnsListofEntitlements() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
                .get("/claim-spiffs/id/"
                		+dummy.getAccountId()
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void updateInvoiceUrl_ReturnsNothing() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
        		.body(dummy.getNewInvoiceUrl())
                .post("/claim-spiffs/entitlements/"
                		+dummy.getSpiffEntitlementWebDto().getPartnerSaleRegistrationId()
                		+"/invoiceurl"
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void updatePartnerRep_ReturnsNothing() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
                .post("/claim-spiffs/entitlements/"
                		+ dummy.getSpiffEntitlementWebDto().getPartnerSaleRegistrationId()
                		+ "/partnerrepuserid/"
                		+ dummy.getNewUserId()
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

}