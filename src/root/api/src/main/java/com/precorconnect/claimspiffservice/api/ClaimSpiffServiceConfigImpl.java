package com.precorconnect.claimspiffservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.database.DatabaseAdapterConfig;
import com.precorconnect.claimspiffservice.identity.IdentityServiceAdapterConfig;
import com.precorconnect.claimspiffservice.okta.OktaAdapterConfig;
import com.precorconnect.claimspiffservice.salesforce.SalesforceAdapterConfig;


public class ClaimSpiffServiceConfigImpl
		implements ClaimSpiffServiceConfig {

    /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;

    private final IdentityServiceAdapterConfig identityServiceAdapterConfig;

    private final SalesforceAdapterConfig salesforceAdapterConfig;

    private final OktaAdapterConfig oktaAdapterConfig;


    /*
    constructors
     */
    public ClaimSpiffServiceConfigImpl(
            @NonNull final DatabaseAdapterConfig databaseAdapterConfig,
            @NonNull final IdentityServiceAdapterConfig identityServiceAdapterConfig,
            @NonNull final SalesforceAdapterConfig salesforceAdapterConfig,
            @NonNull final OktaAdapterConfig oktaAdapterConfig
    ) {

    	this.databaseAdapterConfig =
                guardThat(
                        "databaseAdapterConfig",
                         databaseAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapterConfig =
                guardThat(
                        "identityServiceAdapterConfig",
                        identityServiceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.salesforceAdapterConfig =
                guardThat(
                        "salesforceAdapterConfig",
                        salesforceAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oktaAdapterConfig =
                guardThat(
                        "oktaAdapterConfig",
                        oktaAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();
    }

    /*
    getter methods
    */
    @Override
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {
        return databaseAdapterConfig;
    }

    @Override
    public IdentityServiceAdapterConfig getIdentityServiceAdapterConfig() {
        return identityServiceAdapterConfig;
    }

    @Override
    public SalesforceAdapterConfig getSalesforceAdapterConfig() {
        return salesforceAdapterConfig;
    }

	@Override
	public OktaAdapterConfig getOktaAdapterConfig() {

		return oktaAdapterConfig;
	}

}
