package com.precorconnect.claimspiffservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;



class GuiceModule extends
        AbstractModule {

    private final DatabaseAdapter databaseAdapter;

    private final IdentityServiceAdapter identityServiceAdapter;

    private final SalesforceAdapter salesforceAdapter;

    private final OktaAdapter oktaAdapter;

    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter,
            @NonNull final SalesforceAdapter salesforceAdapter,
            @NonNull final OktaAdapter oktaAdapter
            ){
    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                         identityServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.salesforceAdapter =
                guardThat(
                        "salesforceAdapter",
                        salesforceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.oktaAdapter =
                guardThat(
                        "oktaAdapter",
                        oktaAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(IdentityServiceAdapter.class)
        		.toInstance(identityServiceAdapter);

        bind(SalesforceAdapter.class)
				.toInstance(salesforceAdapter);

        bind(OktaAdapter.class)
				.toInstance(oktaAdapter);

    }

    private void bindFeatures() {

        bind(ListClaimSpiffWithIdsFeature.class)
        		.to(ListClaimSpiffWithIdsFeatureImpl.class);

        bind(AddClaimSpiffFeature.class)
				.to(AddClaimSpiffFeatureImpl.class);

        bind(ListSpiffEntitlementFeature.class)
        		.to(ListSpiffEntitlementFeatureImpl.class);

        bind(CreateSpiffEntitlementFeature.class)
				.to(CreateSpiffEntitlementFeatureImpl.class);

        bind(UpdatePartnerRepFeature.class)
				.to(UpdatePartnerRepFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
				.to(UpdateInvoiceUrlFeatureImpl.class);

		bind(ListSpiffLogsFeature.class)
				.to(ListSpiffLogsFeatureImpl.class);

		bind(ListSpiffLogsWithIdFeature.class)
				.to(ListSpiffLogsWithIdFeatureImpl.class);

		bind(UpdateSpiffLogForApproveFeature.class)
				.to(UpdateSpiffLogForApproveFeatureImpl.class);

		bind(UpdateSpiffLogForRejectFeature.class)
				.to(UpdateSpiffLogForRejectFeatureImpl.class);

    }
}
