package com.precorconnect.claimspiffservice.salesforce;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.claimspiffservice.core.SalesforceAdapter;

public class SalesforceAdapterImpl
        implements SalesforceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public SalesforceAdapterImpl(
            @NonNull SalesforceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public String getAccountNameWithId(
            @NonNull AccountId accountId
    ) {

        return
                injector
                        .getInstance(GetAccountNameWithIdFeature.class)
                        .execute(accountId);

    }


}
