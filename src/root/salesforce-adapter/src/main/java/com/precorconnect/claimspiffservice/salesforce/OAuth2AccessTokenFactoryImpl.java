package com.precorconnect.claimspiffservice.salesforce;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

@Singleton
class OAuth2AccessTokenFactoryImpl
        implements OAuth2AccessTokenFactory {

    /*
    fields
     */
    private final WebTarget salesforceOAuth2TokenWebTarget;

    private final Form createOAuth2AccessTokenRequestForm;
    
    private final SalesforceAdapterConfig salesforceAdapterConfig;

    /*
    constructors
     */
    @Inject
    public OAuth2AccessTokenFactoryImpl(
            @NonNull @InjectSalesforceOAuth2WebTarget WebTarget salesforceOAuth2WebTarget,
            @NonNull SalesforceAdapterConfig salesforceAdapterConfig
    ) {

        this.salesforceOAuth2TokenWebTarget = 
				guardThat(
						"salesforceOAuth2WebTarget",
						salesforceOAuth2WebTarget
				)
						.isNotNull()
						.thenGetValue()
						.path("token");

        this.salesforceAdapterConfig = 
				guardThat(
						"salesforceAdapterConfig",
						salesforceAdapterConfig
				)
						.isNotNull()
						.thenGetValue();

        createOAuth2AccessTokenRequestForm = new Form();

        createOAuth2AccessTokenRequestForm
                .param("grant_type", "password");

        createOAuth2AccessTokenRequestForm
                .param(
                        "client_id",
                        salesforceAdapterConfig
                                .getConnectedAppCredentials()
                                .getClientId()
                                .getValue()
                );

        createOAuth2AccessTokenRequestForm
                .param(
                        "client_secret",
                        salesforceAdapterConfig
                                .getConnectedAppCredentials()
                                .getClientSecret()
                                .getValue()
                );

        createOAuth2AccessTokenRequestForm
                .param(
                        "username",
                        salesforceAdapterConfig
                                .getUsername()
                                .getValue()
                );

        createOAuth2AccessTokenRequestForm
                .param(
                        "password",
                        salesforceAdapterConfig
                                .getPassword()
                                .getValue()
                );

    }

    @Override
    public OAuth2AccessToken construct() {

        CreateOAuth2AccessTokenResponse createOAuth2AccessTokenResponse =
                salesforceOAuth2TokenWebTarget
                        .request(MediaType.APPLICATION_JSON)
                        .post(
                                Entity.entity(
                                        createOAuth2AccessTokenRequestForm,
                                        MediaType.APPLICATION_FORM_URLENCODED
                                ),
                                CreateOAuth2AccessTokenResponse.class
                        );


        return
                new OAuth2AccessTokenImpl(
                        createOAuth2AccessTokenResponse.getAccess_token()
                );

    }

}
