package com.precorconnect.claimspiffservice.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();
}
