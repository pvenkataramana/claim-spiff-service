package com.precorconnect.claimspiffservice.okta;

import org.checkerframework.checker.nullness.qual.NonNull;
import static com.precorconnect.guardclauses.Guards.guardThat;


class OktaApiTokenImpl
        implements OktaApiToken {

    /*
    fields
     */
    private final String value;

    /*
    constructors
     */
    public OktaApiTokenImpl(
            @NonNull String value
    ) throws IllegalArgumentException {

    	this.value =
                guardThat(
                        "value",
                         value
                )
                        .isNotNull()
                        .hasCharacterLengthGreaterThan(0)
                        .thenGetValue();
                        
    }

    /*
    getter methods
     */
    @Override
    public String getValue() {
        return value;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OktaApiTokenImpl that = (OktaApiTokenImpl) o;

        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
