package com.precorconnect.claimspiffservice.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId;

public interface AddClaimSpiffFeature {

	Collection<ClaimSpiffId> addClaimSpiffs(
			@NonNull Collection<ClaimSpiffDto> listClaimSpiffs
			) throws AuthenticationException;

}
