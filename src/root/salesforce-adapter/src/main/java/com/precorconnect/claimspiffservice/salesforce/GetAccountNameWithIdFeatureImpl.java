package com.precorconnect.claimspiffservice.salesforce;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;

@Singleton
class GetAccountNameWithIdFeatureImpl
        implements GetAccountNameWithIdFeature {

    /*
    fields
     */
    private final OAuth2AccessTokenFactory oAuth2AccessTokenFactory;

    private final WebTarget salesforceServicesDataV34SObjectsAccountWebTarget;


    /*
    constructors
     */
    @Inject
    public GetAccountNameWithIdFeatureImpl(
            @NonNull OAuth2AccessTokenFactory oAuth2AccessTokenFactory,
            @InjectSalesforceServicesDataV34WebTarget
            @NonNull
            WebTarget salesforceServicesDataV34WebTarget
    ) {

        this.oAuth2AccessTokenFactory =
				guardThat(
						"oAuth2AccessTokenFactory",
						oAuth2AccessTokenFactory
				)
						.isNotNull()
						.thenGetValue();

        salesforceServicesDataV34SObjectsAccountWebTarget =
				guardThat(
						"salesforceServicesDataV34WebTarget",
						salesforceServicesDataV34WebTarget
				)
						.isNotNull()
						.thenGetValue()
						.path("sobjects/Account");

    }

    /*
    execution methods
    */
    @Override
    public String execute(
            @NonNull AccountId accountId
    ) {

        OAuth2AccessToken accessToken =
                oAuth2AccessTokenFactory.construct();

        String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );

        CommercialAccountView accountView =
                salesforceServicesDataV34SObjectsAccountWebTarget
                        .path(accountId.getValue())
                        .queryParam("fields", "Id,Name")
                        .request(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorizationHeaderValue)
                        .get(CommercialAccountView.class);

        return
        		accountView.getName();

    }
}
