package com.precorconnect.claimspiffservice.salesforce;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
class CommercialAccountView {

    /*
    fields
     */
    private final String Id;

    private final String Name;


    /*
    constructors
     */
    public CommercialAccountView(
            @NonNull String Id,
            @NonNull String Name
    ) {

        this.Id =
				guardThat(
						"Id",
						Id
				)
						.isNotNull()
						.thenGetValue();

        this.Name =
				guardThat(
						"Name",
						Name
				)
						.isNotNull()
						.thenGetValue();

    }

    /*
    getter methods
     */
    public String getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    /*
    equality methods
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommercialAccountView other = (CommercialAccountView) obj;
		if (Id == null) {
			if (other.Id != null) {
				return false;
			}
		} else if (!Id.equals(other.Id)) {
			return false;
		}
		if (Name == null) {
			if (other.Name != null) {
				return false;
			}
		} else if (!Name.equals(other.Name)) {
			return false;
		}
		return true;
	}

}
